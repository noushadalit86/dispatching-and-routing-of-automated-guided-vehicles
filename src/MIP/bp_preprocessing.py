import bp_helpers as bp_h
#from .. import helperclasses
from itertools import permutations
import intervaltree
import math
import time
import random

'''

params:
list of vehicles
list of tasks (with setup and processing times)

generates all feasible schedules.
output params
a : ikj if task j is in schedule k for machine i
b : ikjl if schedule k on machne i shares a charge on l
[list of schedules, a, b]
'''

def allschedulessize(n,m,k):
    return sum([(math.factorial(n)/math.factorial(n-x)) for x in range(k+1)])*m

def generateallschedules(instance, tasklimitrange,sampling,samplingrange,fullrecharge,offsetting,depotend,startsol,seed,timelimit):
    #for each vehicle, gen all combinations
    #If a charge is scheduled, update the array of start/ends for charges
    #  (k, start,end) for each machine, then build 1D interval tree after all schedules
    #for the machine has been generated(m 1D interval trees).
    random.seed(seed)
    runstart = time.time()
    schedules = [] #m x feasible schedules
    a = [] # m x feasible x n
    b = [] #calB set of bphi
    stations = range(instance.stations)
    for i in range(instance.stations):
         b.append([])
    c = []
    n = instance.n
    m = instance.m
    arrival = instance.starttimes
    duedates = instance.duedates
    modes = instance.modes
    allschedules = []
    tasklist = [x for x in range(n)]
    taskprvehicle = int(math.ceil(n/float(m)))
    charges = []
    #timeprcharge = instance.charget/float(instance.chargea)
    maxtime = instance.maxtime
    batterytree = []#intervaltree.IntervalTree()
    #tasklimit as parameter instead of computing within function
    if tasklimitrange[1] > n:
        tasklimitrange[1] = n
    
    #init
    for i in range(m):
        allschedules.append([])
        a.append([])
        #b.append([])
        c.append([])

    #Generate permutations

    #generate simple feasible solution by splitting single perm between vehicles
    if startsol:
        templist = list(tasklist)
        random.shuffle(templist)
        perms = [templist]
    else:
        perms = []
    if sampling: #or if allschedulessie(n,m,tasklimit)>10000
        print "Sampling {}".format(sampling)
        low = samplingrange[0]
        high = samplingrange[1]
        if abs(low) >taskprvehicle:
            low = -taskprvehicle +1
        if high+taskprvehicle >n:
            high = n-taskprvehicle  
        for i in range(sampling):
            chance = random.randint(low,high)
            perms.append(random.sample(tasklist,taskprvehicle+chance))

    else:
        #print "Generating {}".format(allschedulessize(n,m,tasklimit))
        for r in range(tasklimitrange[0],tasklimitrange[1]+1):
            perms += permutations(tasklist,r)
    print "Generated permulations time:{}".format(time.time()-runstart)

    startsolution = []
    startcharges = []
    for i in perms:
        if (time.time()-runstart) >(timelimit/6.5) :
                break
        if len(i)==n:
            rest = n%m
            if rest >0:               
                ps = []
                index = 0
                for j in range(m):
                    if j >= rest:
                        ps.append(i[index:index+taskprvehicle])
                        index += taskprvehicle
                    else:
                        ps.append(i[index:index+taskprvehicle-1])
                        index += taskprvehicle-1
            else:
                ps = [i[x:x+taskprvehicle] for x in range(0, len(i), taskprvehicle)]
                
        else:
            ps = i
        for j in range(m):
                if len(i) ==n:
                    vtasks = ps[j]
                else:
                    vtasks = list(ps)
                level = instance.blevels[j]
                full = instance.fullb
                minb = instance.minb
                schedule = []
                starttime = instance.mstarttimes[j]
                a[j].append([0]*(n+2))
                #b[j].append([])
                scheduleindex = len(allschedules[j])
                prev = -1
                feasible = True
                for k,w in enumerate(vtasks):
                    #REDO: if setup times change for 3d setups
                    #index into vehicle, then the tasks
                    ptime = instance.ptimes[w+1]
                    consumption = instance.consumption[w+1] # offset by one, because instance is one indexed

                    setup = instance.stimes[prev+1][w+1]
                    #Either force recharge on dip below min or force on dip below amount required for task
                    tlevel = level -consumption
                    if tlevel < full/2 or level < consumption:
                        docharge = random.randint(1,1)
                        if level < consumption or tlevel < minb or docharge==1:
                            station = random.randint(stations[0],stations[-1])
                            #calculate needed recharge
                            amount = 0
                            if fullrecharge:
                                amount = full -level
                            else:
                                for t in vtasks[k:]:
                                    amount += instance.consumption[t+1]
                                    if level + amount > full:
                                        amount = full-level
                                        break
                            #calculate time
                            lmode = None
                            amode = None
                            for t in modes:
                                if t.inTMode(level):
                                    lmode = t
                                if t.inTMode(level+amount):
                                    amode = t
                                    break
                            ssetup = instance.stimes[prev+1][n+1]
                            #sptime = instance.charget #rest*timeprcharge
                            sptime = int(math.ceil(amode.getTime(level+amount)-lmode.getTime(level)))#difference is processing time
                            level = level + amount
                            starttime += ssetup
                            end = starttime+ sptime
                            cindex = len(schedule) #index in schedule
                            charge = [starttime,end,(j,scheduleindex),station,cindex]
                            schedule.append({'index':(n+len(charges)+1),'start':starttime,'setup':ssetup,'ptime':sptime,'station':station,'object':charge})
                            
                            if len(i)==n:
                                startcharges.append(charge)
                            else:
                                charges.append(charge)
                            '''
                            b[j][-1].append([0*maxtime])
                            for t in range(starttime+ssetup,end):
                                b[j][-1][-1][t] = 1
                            '''
                            starttime = end
                            setup = instance.stimes[n+1][w+1]
                    #elif k == 0 and v.setup != -1:
                    #    setup = v.setup
                    starttime += setup
                    if starttime < arrival[w+1]:
                        starttime = arrival[w+1]
                    if starttime + ptime> duedates[w+1]:
                        feasible = False
                        break
                    level -= consumption
                    schedule.append({'index':w+1,'start':starttime,'setup':setup,'ptime':ptime})
                    a[j][scheduleindex][w+1]= 1
                    starttime += ptime
                    end = starttime
                    prev = w
                if feasible:
                    if depotend:
                        #Add final recharge to schedule
                        station = random.randint(stations[0],stations[-1])
                        #calculate needed recharge - always full at end
                        amount = full -level
                        #calculate time
                        lmode = None
                        amode = None
                        for t in modes:
                            if t.inTMode(level):
                                lmode = t
                            if t.inTMode(level+amount):
                                amode = t
                                break
                        ssetup = instance.stimes[prev+1][n+1]
                        #sptime = instance.charget #rest*timeprcharge
                        sptime = int(math.ceil(amode.getTime(level+amount)-lmode.getTime(level)))#difference is processing time
                        level = level + amount
                        '''
                        if len(i) == n :
                            #if (k < (8-n%m) and k>=instance.stationlimit) or (k >= (8-n%m) and k-(8-n%m)>=(instance.stationlimit)):
                            if k > 8-n%m:
                                starttime += 1*sptime
                        '''   
                        starttime += ssetup
                        end = starttime + sptime
                        cindex = len(schedule) #index in schedule
                        charge = [starttime,end,(j,scheduleindex),station,cindex]
                        schedule.append({'index':(n+len(charges)+1),'start':starttime,'setup':ssetup,'ptime':sptime,'station':station,'object':charge})
                        
                        
                        if len(i) == n:
                            startcharges.append(charge)
                        else:
                            charges.append(charge)
                        starttime = end
                        
                    if len(i)==n:
                        startsolution.append((j,scheduleindex))
                        
                    c[j].append(starttime)
                    allschedules[j].append(schedule)
                else:
                    #if schedule is not feasible, cleanup!
                    del a[j][scheduleindex]
                    for ch in range(len(charges)-1,-1,-1):
                        if charges[ch][2][0]!= j or charges[ch][2][1] != scheduleindex:
                            break
                        del charges[ch]          
    if len(charges)>0:
        #Maybe check size here and apply offsetting if > b and param set
        if startsol and len(startcharges) >0:
            curindex = 0
            startcharges.sort(key=lambda x: (x[0],x[1]))
            nrgroups = m/instance.stationlimit
            print startcharges
            maxc = startcharges[0][1]
            maxp = startcharges[0][1] - startcharges[0][0]
            #groups = [0]*nrgroups
            overlaps = [0]*instance.stationlimit
            curindex = 0
            while not (curindex == len(startcharges)):
                for k in startcharges[curindex:]:
                    curindex +=1
                    
                    if maxc < k[0]:
                        #reset overlaps and maxp because we are in new group
                        overlaps=  [0]*instance.stationlimit
                        maxp = k[1] - k[0]
                    elif overlaps[k[3]]+1 >instance.stationlimit:
                        schedule = allschedules[k[2][0]][k[2][1]]
                        vindex = k[2][0]
                        cindex = k[4]
                        charge = schedule[cindex]
                        offset = maxc-k[0]#int(math.ceil(maxp*1))
                        #charge['start'] = charge['start'] + offset
                        index = cindex+1
                        feasible = True
                        for i in schedule[cindex:]:
                            i['start']+= offset
                            end = i['start'] + i['ptime']
                            if i['index']>n:
                                i['object'][0]+= offset
                                i['object'][1]+= offset
                            if end> duedates[i['index']]:#check feasibility wrt deadline
                                feasible = False
                                break
                            index +=1
                        if not feasible:#do cleanup
                            for ch in range(len(startcharges)-1,-1,-1):
                                if startcharges[ch][2][0]!= vindex or startcharges[ch][2][1] != scheduleindex:
                                    break
                                del startcharges[ch]
                        else:
                            end = charge['start'] + charge['ptime']
                            k[0] += offset
                            k[1] += offset
                            
                            c[vindex][k[2][1]]+=offset
                        
                        break
                    if maxp < (k[1]-k[0]):
                        #Only update maxp when past offset
                        maxp = k[1] - k[0]    
                    if k[1]>maxc:
                        maxc = k[1]
                    overlaps[k[3]]+=1
                tempunsorted = startcharges[curindex:]
                startcharges = startcharges[:curindex]
                tempunsorted.sort(key=lambda x: (x[0],x[1]))
                startcharges = startcharges + tempunsorted
            for i in startcharges:
                charges.append(i)
        if offsetting :
            print "Offsetting"
            clones = random.sample(charges,len(charges)/3)
            for k in clones:
                if (time.time()-runstart) >(timelimit/6.0) :
                    break
                schedule = list(allschedules[k[2][0]][k[2][1]])
                scheduleindex = len(allschedules[k[2][0]])
                vindex = k[2][0]
                cindex = k[4]
                charge = schedule[cindex]
                offset = int(math.ceil(charge['ptime']*(random.random()*offsetting)))
                charge['start'] = charge['start'] + offset
                index = cindex
                feasible = True
                end = 0
                for i in schedule[cindex:]:
                    i['start']+= offset
                    end = i['start'] + i['ptime']
                    if i['index'] > n:
                        if i['index'] ==charge['index']:
                            charges.append((i['start'],end,(vindex,scheduleindex),charge['station'],index))
                        charges.append((i['start'],end,(vindex,scheduleindex),i['station'],index))
                    elif end> duedates[i['index']]:#check feasibility wrt deadline
                        feasible = False
                        break
                    index +=1
                if not feasible:#do cleanup
                    for ch in range(len(charges)-1,-1,-1):
                        if charges[ch][2][0]!= vindex or charges[ch][2][1] != scheduleindex:
                            break
                        del charges[ch]
                else:
                    a[vindex].append(list(a[vindex][k[2][1]]))
                    allschedules[vindex].append(schedule)
                    #end = charge['start'] + charge['ptime']
                    c[vindex].append(end)
                    #c[vindex].append(c[vindex][k[2][1]]+offset)
        nrsched = 0
        for i in allschedules:
            nrsched += len(i)
        print nrsched
        #ready for conflict check
        print "Enter charges "+str(len(charges))
        for i in charges:
            i = tuple(i)
        charges.sort(key=lambda x: x[0])
        allch = []
        for i in charges:
            #print i
            starttime = i[0]
            end = i[1]
            if starttime == end:
                continue
            allch.append((starttime,end,(starttime,end,(i[2][0],i[2][1]),i[3])))
        batterytree = intervaltree.IntervalTree.from_tuples(allch)
    print "Check overlaps"
    #now make a vert for each interval, and query for its neighbours (forwards)
    start = -1
    stat = -1
    ovt = time.time()
    for i,v in enumerate(charges):
        if(time.time()-runstart)>=(timelimit):
            break
        if v[0] == start and v[3] == stat:
            continue
        #query using starttime
        res = batterytree[v[0]]
        if len(res)>instance.stationlimit:
            schedules = []
            #latest startpoint given by sorting
            start = v[0]
            end = v[1]
            stat = v[3]
            for j in res:
                try:
                    if j.data[3]== v[3]:
                        if j.data[1] < end:
                            end = j.data[1]
                        schedules.append(j.data)
                except:
                    print j
                    print v
                    print j.data[1]
            
            #earliest endpoint from sorting on ends
            #schedules.sort(key=lambda x: x[1])
            #save, overlapping interval, schedules and eventually station
            b[stat].append(bp_h.bp_conflicts((start,end),schedules,stat))
    '''
    for i in b:
        for j in i:
            for k in j.schedules:
                print k[2]
    '''
    #If size is above the limit, add as cut
    ovt = time.time() -ovt
    print ovt
    endruntime =  time.time() - runstart
    endruntime = round(endruntime,4)
    print "Generated schedules"
    return (allschedules,a,b,batterytree,c, endruntime,startsolution)

def UBRecharges(instance):
    taskspervehicle = math.ceil(sum([i for i in instance.consumption])/float(instance.m))
    ub = taskspervehicle/(instance.fullb-instance.minb)
    return int(math.ceil(ub))

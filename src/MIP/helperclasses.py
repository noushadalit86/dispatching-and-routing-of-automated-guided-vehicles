# -*- coding: utf-8 -*-
import math, os, sys
from MessageHandler import *

class Content():
    def __init__(self,contents):
        self.contents = contents
        self.line = contents[0]
        self.i = 0
    def nextLine(self):
        if self.i < len(self.contents)-1:
            self.i = self.i + 1
            self.line = self.contents[self.i]
            return self.line
        return ""

class Task(object):
    def __init__(self,index, kind, start, end, origin, dest, ptime,entry):
        self.index = index
        self.kind = kind
        self.start = start
        self.end = end
        self.origin = origin
        self.dest = dest
        self.ptime = ptime
        self.assignedTo = None
        self.entry = entry
        self.consumption = getConsumption(ptime)
        #set later
        self.started = False
        self.setup = 0 #before doing task, started = prev.started + prev.ptime + setup
        self.waiting = 0
    def toString(self):
        s = str(self.index)+" k:"+str(self.kind)+" st:"+str(self.start)+" :"+str(self.end)
        s = s + " o:"+str(self.origin)+" d:"+str(self.dest)+" p:"+str(self.ptime)
        s = s+"  en:"+str(self.entry)+" c:"+str(self.consumption)
        if self.assignedTo != None:
            s = s +" "+str(self.assignedTo)
        return s
    def preemptCost(self,time):
        if self.started:
            processing = self.started + self.setup + self.ptime
            if self.started + self.setup < time and processing > time:
                return processing - time
            
        return 0

def createRechargeTask(starttime,ptime,setup,taskoverview):
    rtask = Task(-1,'c',starttime,0,taskoverview.stations[0],taskoverview.stations[0],ptime,0)
    rtask.started = starttime
    rtask.setup = setup
    return rtask

'''
def getFullb():
    return 100

def getMinb():
    return int(round(getFullb()/2))

def getChargea():
    return int(round(getFullb()/2))

def getCharget():
    return int(round((getFullb()-getMinb())-((getFullb()-getMinb())*0.75)))
'''
def getConsumption(ptime):
    return int(round(ptime*1.55))

class Vehicle(object):
    def __init__(self,index,pos, batterylevel,full):
        self.pos = pos
        self.index = index
        self.batterylevel = batterylevel
        self.full = full
        self.tasks = []
        self.modes = []
    def toString(self):
        s = str(self.index)+" "+str(self.pos)+" "+str(self.batterylevel)+"\n Tasks:"
        for i in self.tasks:
            s = s + "\n"+str(i.index)+ " k"+str(i.kind)+ " start: "+str(i.started)
        return s
    def getBatteryLevel(self,time):
        level = self.full
        #ch = getCharget()/float(getChargea())
        for i in self.tasks:
            if i.started < time:
                if i.kind == 'c':
                    #get actual amount of recharge by converting from relative time
                    amode = None
                    lmode = None
                    for t in self.modes:
                        if t.inTMode(level):
                            lmode = t
                    totaltime = lmode.getTime(level) + i.ptime

                    for t in self.modes:        
                        if t.inAMode(totaltime):
                            amode = t
                    level = t.getAmount(totaltime)
                    if level > self.full:
                        level = self.full
                else:
                    level = level - i.consumption
            else:
                return int(round(level))
        return int(round(level))
    def currentTask(self,time):
        #What am I doing - Vehicle
        task = False
        index = 0
        for i in self.tasks:
            if i.started < time:
                task = i
            else:
                break
            index = index +1
        '''
        if task:
            #If task should be finished
            #leave out since it would be more accurate to update vehicle pos anyways
            if task.started + task.ptime + task.setup < time:
                task = False
        '''
        return task, index
    def clean(self,currenttime):
        index = len(self.tasks)
        for i,t in enumerate(self.tasks):
            if t.started > currenttime:
                index = i
                break
        #remove all which may have been rescheduled
        if index < len(self.tasks):
            self.tasks[index:] = []
    
class Results(object):
    def __init__(self,machines,makespan, time, upper, lower):
        self.machines = machines
        self.makespan = makespan
        self.runtime = time
        self.upper = upper
        self.lower = lower
    def output(self):
        s = "makespan: "+str(self.makespan)+"\n" 
        s = s + "time: "+str(self.runtime)+"\n"
        s = s + "UB: "+str(self.upper)+"\n"
        s = s + "LB: "+str(self.lower)+"\n"
        tmp = ""
        for i in self.machines:
            s = s + str(i['index'])+":\n"
            tmp = ""
            for j in i['tasks']:
                s = s + tmp +str(j)

                tmp = " "
            s = s + "\n"

        return s
                
        
class Station(object):
    def __init__(self, index, pos):
        self.pos = pos
        self.index = index
        self.docked = []
    def toString(self):
        s = str(self.index)+" "+self.pos.toString()
            
class TaskOverview(object):
    def __init__(self, tasks, garbage, linen, misc, machines, stations, graph,limits,full,minb,chargea,charget):
        self.tasks = tasks
        self.garbage = garbage
        self.linen = linen
        self.misc = misc
        self.machines = machines
        self.stations = stations
        self.stationlimits = limits
        self.graph = graph
        self.currenttime = 0
        self.currenttask = 0
        self.fullb = full
        self.minb = minb
        self.chargea = chargea
        self.charget = charget
        self.stimes = []
        self.chargeindex = -1
        self.ctasks = []
    def getCIndex(self):
        self.chargeindex = self.chargeindex + 1
        return self.chargeindex -1
    def setTime(self,time):
        self.currenttime = time
        self.currenttask = findNextTask(time)
    def findNextTask(self,time):
        index = 0
        for i in self.tasks:
            if i.entry >= time:
                return index
            index = index + 1
        return
    def getNextTasks(self):
        
        if self.currenttask >= len(self.tasks):
            self.currenttask = -1
            tasks = []
            nextTask = self.tasks[self.currenttask]
            self.currenttime = nextTask.entry
            return False
        else:
            nextTask = self.tasks[self.currenttask]
            tasks = [nextTask]
            self.currenttime = nextTask.entry
        printMessage("current time: "+str(self.currenttime),self.currenttime, False)
        #print self.tasks[self.currenttask].toString()
        for i in self.tasks[:self.currenttask]:
            #Check if any task could be rescheduled
            if i.started:#if we have a starttime
                if i.started > self.currenttime:#if not yet started
                    printMessage("this task has not been started", self.currenttime)
                    printMessage(i.toString(), self.currenttime)
                    tasks.append(i)
        #print nextTask.toString()
        #print nextTask.entry
        #print "currenttime: "+str(self.currenttime)
        #print "currenttaskindex: "+str(nextTask.index)
        if nextTask.entry >= self.currenttime:
            nextcurrent = 1
            
            for i in self.tasks[self.currenttask:]:
                #print "task greater or eq"+str(i.toString())
                if i == nextTask:
                    continue
                if i.entry != nextTask.entry:
                    break
                tasks.append(i)
                nextcurrent = nextcurrent+1
                
            self.currenttime = nextTask.entry
            self.currenttask = self.currenttask + nextcurrent
            
            #print "new time "+str(self.currenttime)
            #print "next currentatsk: "+str(self.tasks[self.currenttask].toString())
            if self.currenttask == len(self.tasks):
                self.currenttask = self.currenttask+1
        else:
            nextcurrent = 1
            for i in self.tasks:
                if i.index == nextTask.index:
                    break
                if i.entry > self.currenttime:
                    break
                tasks.append(i)
                nextcurrent = nextcurrent+1
            
            self.currenttask = self.currenttask + nextcurrent + 1
            self.currenttime = self.tasks[self.currenttask].entry
            #print "new time "+str(self.currenttime)
        if len(tasks) >= 1:
            #print self.currenttask
            s = ""
            for i in tasks:
                s = s + str(i.index)+ " "
            #print s
            return tasks
        else:
            return False
            
class Mode():
    def __init__(self,start,end,slope,intercept):
        self.start = start
        self.end = end
        self.slope = slope
        self.intercept = intercept
        if slope == None or intercept == None:
            self.calcMode()
    def getTime(self,x):
        return self.slope*x + self.intercept
    def getAmount(self,y):
        return (y-self.intercept)/self.slope
    def calcMode(self):
        x = self.start[0]
        y = self.start[1]
        xx = self.end[0]
        yy = self.end[1]
        self.slope = (y-yy)/float((x - xx))
        self.intercept= y-self.slope*x
    def inTMode(self,value):
        return self.start[0] <= value <= self.end[0]
    def inAMode(self,value):
        return self.start[1] <= value <= self.end[1]
        
def generateModes(modes,full,chargea,charget):
        if modes == None or len(modes)==0: #default
            ratio = charget/float(chargea)
            modes = [Mode((0,0),(full,int(full*ratio)),None,None)]
        elif isinstance(modes[0],Mode):
            return modes
        else:
            nmodes = []
            for i in modes: #convert from instance
                print i
                nmodes.append(Mode((i[0],i[1]),(i[2],i[3]),i[4],i[5]))
        return modes

class Instance():
    def __init__(self,ptimes,stimes,starttimes,duedates,blevels,consumption,fullb,minb,charget, chargea,modes):
        self.ptimes = ptimes #j
        self.n = len(self.ptimes)-2
        self.stimes = stimes #jk
        self.starttimes = starttimes
        self.duedates = duedates #j
        self.maxtime = sorted(self.duedates)[-1]
        self.blevels = blevels #i
        self.m = len(self.blevels)
        self.M = 0
        index = 0
        for i in self.stimes:
            for k in i:
                self.M = self.M + k*ptimes[index]
            index = index + 1
        if self.M > 2147483644:
            self.M = 2147483644
        self.consumption = consumption #j
        self.fullb = fullb #all i
        self.minb = minb #all i
        self.charget = charget #all i
        self.chargea = chargea
        self.mstarttimes = []
        self.stations = 1
        self.stationlimit = 5
        self.modes = modes
        self.modes= generateModes(self.modes,fullb,chargea,charget) # if empty then limits are 0 to max
    
    def instanceToString(self):
        s = "-#"
        s = s + "\n"+str(self.n)+"\n"+str(self.m)
        #Processing times
        s = s + "\n"+ "-p"+"\n"
        for i in self.ptimes:
            s = s +str(i)+ "\n"
        #Large Integer
        s = s + "-M"+"\n"
        s = s + str(self.M)+"\n"
        #Starttime
        s = s + "-st"+"\n"
        for i in self.starttimes:
            s = s +str(i)+ "\n"
        #Duedates
        s = s + "-d"+"\n"
        for i in self.duedates:
            s = s +str(i)+ "\n"
        #Setup times ij tasks
        s = s + "-s"+"\n"
        space = ""
        for i in self.stimes:
            for j in i:
                s = s +space+str(j)
                if True:
                    space = " "
            space = ""
            s = s +"\n"
        #Battery
        s = s + "-b"+"\n"+str(self.fullb)+"\n"+str(self.minb)+"\n"+str(self.charget)+"\n"
        s = s + str(self.chargea)+"\n"
        #Levels
        s = s + "-l"+"\n"
        for i in self.blevels:
            s = s +str(i)+ "\n"
        #Consumption
        s = s + "-c"+"\n"
        for i in self.consumption:
            s = s +str(i)+ "\n"
        if len(self.mstarttimes) >0: 
            s = s + "-mst"+"\n"
            for i in self.mstarttimes:
                s = s +str(i)+ "\n"
        s = s + "-re\n{}\n{}".format(self.stations,self.stationlimit)
        return s

class Template(object):
    def __init__(self,linen, garbage,misc,machines,battery,graph,tasks):
        self.linen = linen
        self.garbage = garbage
        self.misc = misc
        self.tasks = tasks
        self.machines = machines
        self.battery = battery
        self.graph = graph
        self.stimes = []
        

class Node(object):
    def __init__(self,x,y,kind):
        self.x = x
        self.y = y
        self.xd = -1
        self.yd = -1
        self.pathpoint = None
        self.dest = None
        self.kind = kind
        self.taskkind = ""
        self.ptime = 0
    


class Point(object):
  def __init__(self,x,y):
    self.x = x
    self.y = y
    self.neighbours = set([])
    self.sol = False
    self.index = -1
  def toString(self):
      return "("+str(self.x)+","+str(self.y)+")"
  def display(self):
    print self.toString()
  def dist(self,p):
      return math.sqrt(math.pow(self.x-p.x,2)+math.pow(self.y-p.y,2))

#Functions
def writeToFile(fname,s): #seems kinda excess
    if os.path.isfile(fname):
        print "Name '"+fname+"' is already taken!"
        return False
    try:
        with open(fname,'w') as f:
            f.write(s)
        return True
    except:
        print "Something went wrong opening the file "+fname+"..."
        os.remove(fname)
        sys.exit(0)

def decimal(nr,dec):
    nr = nr*(10**dec)
    nr = int(nr)
    nr = float(nr)/(10**dec)
    return nr
        
    

def overwriteFile(fname,s):
    try:
        with open(fname,'w') as f:
            f.write(s)
        return True
    except:
        print "Something went wrong opening the file "+fname+"..."
        os.remove(fname)
        sys.exit(0) 


def appendToFile(fname,s):
    try:
        with open(fname,'a') as f:
            f.write(s)
        return True
    except:
        print "Something went wrong opening the file (File doesnt exist?) "+fname+"..."
        os.remove(fname)
        sys.exit(0)
def addTikzHeader(fname,caption,label):
    s = "\\begin{figure}[H]"
    s = s + "\label{fig:"+label+"}\n"
    s = s + "\caption{"+caption+"}\n"
    s = s + "\\begin{tikzpicture}\n"
    return overwriteFile(fname,s)

def addTikzFooter(fname):
    s = "\end{tikzpicture}\n"
    s = s + "\end{figure}"
    return appendToFile(fname,s)


def addTableHeadToFile(fname, cols,caption, label):
    s = "\\begin{table}[]\n"
    s = s + "\centering\n"
    s = s + "\caption{"+caption+"}\n"
    s = s + "\label{"+label+"}\n"
    s = s + "\\begin{tabular}{"+'|l'*cols+"|"+"}\n"
    s = s + "\hline\n"
    
    return overwriteFile(fname,s)

def addTableFootToFile(fname):
    s = "\end{tabular}\n \end{table}"

    return appendToFile(fname,s)

def expoDist(x,r):
    t = math.pow(1*(1+r),x) 

    return t

def rgen(time,nr):
    return math.pow(time,1/float(nr)) -1

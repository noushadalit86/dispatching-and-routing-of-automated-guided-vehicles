import os
import bp_preprocessing as bp_pre
import bp_model
import finstance
import helperclasses
import bp_helpers as bp_h
import bp_output as bp_o
import math

'''
Params:
inputfile
outputfile
#columns to add
branching strategy


runs branch and price by calling other functions and passing their results


files
bp-preprocessing
bp-mp
bp-pricingproblem
bp-branching
bp-helpers
'''

#have other file init stuff and send instance, tasks and vehicle to this
def runModel(*params):
    instance = params[0]
    tasklimit = params[1]
    problem = initProblem(instance,tasklimit)#generate schedules create model
    sol = []
    while True:
        changes = False
        LR = problem.master.relax()
        LR.optimize()
        #Retrieve dual values
        duals = [c.Pi for c in LR.getConstrs()]
        for i in duals:
            print i
        
        gamma = duals[0:instance.n]
        # skip betas for vehicles
        beta = duals[instance.n: instance.n+instance.m]
        #Check if we can get epsilons,
        if len(duals)> instance.n+instance.m:
            epsilon = duals[instance.n+instance.m:]
        else:
            epsilon = []
        problem.params.gamma = gamma
        problem.params.beta = beta
        problem.epsilon = epsilon
        for k in range(instance.m):
            #may run on all vehicles, then apply betas
            spp,sppx,sppbr = bp_model.initSPP_Single(problem.params,k)
            #Add dual vars to spp
            spp.update()

            spp.optimize()
            #Stop if no columns left to add
            if spp.ObjVal < 1+1.e-6:
                #extract  solution, update batterytrees
                schedule,recharges = extractSPP(spp,sppx,problem,k)
                changes = True
        if not changes:
            break


    return sol, problem

def runPartitioning(instance,tasklimit,makespan,sampling,samplingrange,fullrecharge,offsetting,depotend,startsol,seed,timelimit):
    problem = initSetPartitioning(instance,tasklimit,makespan,sampling,samplingrange,fullrecharge,offsetting,depotend,startsol,seed,timelimit)#generate schedules create model
    problem.master.optimize()
    if problem.master.getAttr("SolCount")  >0:
        solution = bp_h.extractSolution(problem)
    else:
        solution = []
    
    return solution,problem

def run(*params):
    #read instance
    instfile = params[0]
    instance = finstance.parseInstance(instfile)
    #output - test if it exists
    outputfile = params[1]
    helperclasses.appendToFile(outputfile,"")
    tasklimit = instance.n
    if len(params) > 2:
        tasklimit = params[2]
        if len(params)> 3:
            delay = int(params[3])
            makespan = params[4]
            sampling = params[5]
            samplingrange = params[6]
            fullre = params[7]
            offsetting = params[8]
            depotend = params[9]
            startsol = params[10]
            seed = params[11]
            timelimit = params[12]
            tikzout = params[13]
            sol,problem = runPartitioning(instance,tasklimit,makespan,sampling,
                                          samplingrange,fullre,offsetting,depotend,startsol,seed,timelimit)        
            if problem.master.getAttr("SolCount")  >0:
                bp_o.tikz_sol(sol,problem.params,tikzout)
        else:
            sol,problem = runModel(instance,tasklimit)
    bp_o.print_sol(sol,outputfile)
    testline = bp_h.extractTestInfo(problem)
    return testline
    
    

def initProblem(instance, tasklimit):
    #generate the schedules
    columns,a,b,batterytrees,c,runtime = bp_pre.generateallschedules(instance,tasklimit,False,False)
    r = bp_pre.UBRecharges(instance)
    params = bp_h.Params(instance,columns,a,b,c,r,batterytrees,runtime)
    mp,x,cover,restrict = bp_model.initMP(params,True,False)
    return bp_h.Problem(mp,params,x,cover,restrict)

def initSetPartitioning(instance,tasklimit,makespan,sampling,samplingrange,fullrecharge,offsetting,depotend,startsol,seed,timelimit):
    #generate the schedules
    columns,a,b,batterytrees,c,runtime,startsol = bp_pre.generateallschedules(instance,tasklimit,sampling,
                                                                     samplingrange,fullrecharge,offsetting,depotend,startsol,seed,timelimit)
    r = bp_pre.UBRecharges(instance)
    params = bp_h.Params(instance,columns,a,b,c,r,batterytrees,runtime,startsol)
    time = timelimit - runtime
    if time <= 0:
        time = 0
    mp,x,cover,restrict,conflict = bp_model.initMP(params,False,makespan,time)
    return bp_h.Problem(mp,params,x,cover,restrict)

def extractSPP(spp,sppx,problem,k):
    params = problem.params
    N = params.instance.n #nr tasks
    M = params.instance.m
    R = params.instance.stations #Stations
    s = params.instance.stimes #setup time
    b = params.b #conflict limit (each station has b spots)
    p_r = params.instance.charget
    p = params.instance.ptimes
    batterytree = params.trees
    order = [0]*(N+2+R*b)
    time = []
    for i,v in enumerate(sppx):
        for j,w in enumerate(v):
            for z,r in enumerate(w):
                
                if r.getAttr("x")==1:
                    order[i]=j
                    time.append(z) 

    def convert_indice(index,start):
        if index >= N+2:
            if start:
                return 0
            else:
                return N+1
        return index
    
    schedule = []
    charges = []
    i = 0
    starttime = params.instance.mstarttimes[k]
    scheduleindex = len(params.columns[k])
    params.a[k].append([0]*(N+2))
    while i != N+1:
        j = order[i]
        if j > N+1:
            #recharge
            ssetup = s[convert_indice(i,True)][convert_indice(j,False)]
            sptime = p_r
            end = starttime + ssetup + sptime
            charges.append((starttime,end,(k,scheduleindex)))
            schedule.append({'index':-(n+len(charges)),'setup':ssetup,'ptime':sptime})
            batterytree[starttime:end] = (starttime,end,(j,scheduleindex))
            starttime = end
        elif j == N+1:
            #last sink
            i = j
        else:
            #regular
            params.a[k][scheduleindex][j]= 1
            setup = s[convert_indice(i,True)][j]
            ptime = p[j]
            end = starttime + setup + ptime
            schedule.append({'index':j,'setup':setup,'ptime':ptime})
            starttime =end
        i = j
    #print schedule
    params.columns[k].append(schedule)
    params.c[k].append(starttime)

    #check for conflicts, create constrs
    sets = checkConflicts(problem,charges,batterytree)


    # add new column to the master problem
    col = Column()
    #may need to save col indices somewhere...
    for i in range(N):
        if a[k][scheduleindex][i] > 0:
            col.addTerms(a[k][scheduleindex][i], problem.cover[i])
    #restrict schedules
    col.addTerms(1, problem.restrict[k])
    for i in sets:
        col.addTerms(1, problem.conflict[i])
    
    problem.x[k][scheduleindex] = master.addVar(obj=starttime, vtype=GRB.BINARY, name="x[%d]"%schduleindex, column=col)
    master.update()   
    return schedule, charges



def checkConflicts(problem,charges,batterytree):
    #Would update master with constraints cutting away sols with over b
    #conflicting recharges
    #so if a new station is full, a new cut is added
    #while if an old station is more full, a cut is extended with a term
    #so we keep a set of conflicts pairs per station
    conflicts = []#(phi)
    #seems like it should be array of size R and sets of conflict for each station...
    for i,v in enumerate(charges):
        if v[0] == charges[i-1]:
            b[-1].add(v)
            continue
        res = batterytree[v[0]]
        b.append({})
        for j in res:
            b[-1].add(j)
    #should incorperate old b so i can see when new conflicts occur.

    #update params (b and B and stuff)

    #update master with new cuts(Dont add any columns for the cahrges, just return the sets they are in)
    
    
    #may not be so useful since there can be conflicts at different points in time
    if len(conflicts)==0:
        return False

    return conflicts

def test(testfiles,nrtasks,limits,makespan,sampling,samplingrange,fullrecharge,offsetting,depotend,startsol,seed,timelimit,output):
    lines = []
    bp_o.testoutputheader(output,makespan,sampling,offsetting)
    helperclasses.appendToFile("plot"+output,"instance samplesize time_preprocessing time_solving\n")
    for j,w in enumerate(limits):
        for i,v in enumerate(testfiles):
            print "bpout{}-tr{}to{}.txt".format(i,w[0],w[1])
            sam = sampling
            if isinstance(sampling,list):
                sam = sampling[i]
            line = run(v,"bpout{}-{}.txt".format(i,w),w,False,makespan,sam,
                       samplingrange,fullrecharge,offsetting,depotend,startsol,seed,timelimit,"bpouten{}.tikz".format(output[:-4]))
            if not sampling:
                line.tasklimit = w
            else:
                distr = math.ceil(line.n/float(line.m))
                line.tasklimit = (distr+samplingrange[0],distr+samplingrange[1]) #+-1 
            bp_o.testoutputline(line,output)
            helperclasses.appendToFile("plot"+output,"{} {} {} {}\n".format(v,sam,line.genruntime,line.runtime))
            print line.getLine()
            lines.append(line)
    #bp_o.testoutput(lines,output)
    bp_o.testoutputfooter(output)
    
    return


#print run("randinst_10_5test.txt","bpout.txt",4,False,"bpout.tikz").getLine()

testfiles = ["randinst_10_5test.txt","randinst_15_5test.txt",
             "randinst_20_5test.txt","randinst_25_5test.txt",'randinst_15_10test.txt','randinst_20_10test.txt',"randinst_25_10test.txt"]
testfiles2 = ["randinst_10_5_fixedtest.txt","randinst_15_5test.txt",
             "randinst_20_5_fixedtest.txt","randinst_25_5_fixedtest.txt",'randinst_15_10test.txt','randinst_20_10test.txt',"randinst_25_10_fixedtest.txt"]
             
limits = [(10,10)]#[2,3,4,5,6,7,8,9,
nrtasks = [math.ceil(10/5.0),math.ceil(15/5.0),math.ceil(20/5.0),math.ceil(25/5.0),math.ceil(25/10.0)]

#makespan, sampling, offsetting
sampling = [[200]*7,[300]*7,[500]*7,[1000]*7]*2
samplingrange = [-1,1]
fullrecharge = False
depotend = False
obj = ([False]*4)+([True]*4)
seed = 13
startsol = False
outs = ["bp_tests-off3-sam200o1-part-time60m.txt","bp_tests-off3-sam300o1-part-time60m.txt",
        "bp_tests-off3-sam500o1-part-time60m.txt","bp_tests-off3-sam1000o1-part-time60m.txt",
        "bp_tests-off3-sam200o1-part-time60m-makespan.txt","bp_tests-off3-sam300o1-part-time60m-makespan.txt",
        "bp_tests-off3-sam500o1-part-time60m-makespan.txt","bp_tests-off3-sam1000o1-part-time60m-makespan.txt"]
outs2 = ["bp_tests-off3-sam200o1-part-time60mfix.txt","bp_tests-off3-sam300o1-part-time60mfix.txt",
        "bp_tests-off3-sam500o1-part-time60mfix.txt","bp_tests-off3-sam1000o1-part-time60mfix.txt",
        "bp_tests-off3-sam200o1-part-time60mfix-makespan.txt","bp_tests-off3-sam300o1-part-time60mfix-makespan.txt",
        "bp_tests-off3-sam500o1-part-time60mfix-makespan.txt","bp_tests-off3-sam1000o1-part-time60mfix-makespan.txt"]
outs3 = ["bp_tests-off3-sam200o1-part-startsol-time60m.txt","bp_tests-off3-sam300o1-part-startsol-time60m.txt",
        "bp_tests-off3-sam500o1-part-startsol-time60m.txt","bp_tests-off3-sam1000o1-part-startsol-time60m.txt",
        "bp_tests-off3-sam200o1-part-startsol-time60m-makespan.txt","bp_tests-off3-sam300o1-part-startsol-time60m-makespan.txt",
        "bp_tests-off3-sam500o1-part-startsol-time60m-makespan.txt","bp_tests-off3-sam1000o1-part-startsol-time60m-makespan.txt"]
outs4 = ["bp_tests-off3-sam200o1-part-startsol-time60mfix.txt","bp_tests-off3-sam300o1-part-startsol-time60mfix.txt",
        "bp_tests-off3-sam500o1-part-startsol-time60mfix.txt","bp_tests-off3-sam1000o1-part-startsol-time60mfix.txt",
        "bp_tests-off3-sam200o1-part-startsol-time60mfix-makespan.txt","bp_tests-off3-sam300o1-part-startsol-time60mfix-makespan.txt",
        "bp_tests-off3-sam500o1-part-startsol-time60mfix-makespan.txt","bp_tests-off3-sam1000o1-part-startsol-time60mfix-makespan.txt"]


ou = "allsphoff0final.txt"
for i,v in enumerate(outs3[3:]):
    for j in range(1): #repeat dif seeds
        helperclasses.appendToFile(ou,v+"\n")
        test(testfiles,nrtasks,limits,True,sampling[i],samplingrange,fullrecharge,0,depotend,startsol,seed+j,60*60,ou)
ou = "allsphoff0finalfix.txt"
for i,v in enumerate(outs4[3:]):
    for j in range(1): #repeat dif seeds
        helperclasses.appendToFile(ou,v+"\n")
        test(testfiles2,nrtasks,limits,True,sampling[i],samplingrange,fullrecharge,0,depotend,startsol,seed+j,60*60,ou)
#limits = [(1,3),(2, 4),(3, 5),(4, 6),(1,3),(1,3),(2, 4)]
ou = "allsphfinalmake.txt"
for i,v in enumerate(outs[3:]):
    for j in range(1): #repeat dif seeds
        helperclasses.appendToFile(ou,v+"\n")
        test(testfiles,nrtasks,limits,True,sampling[i],samplingrange,fullrecharge,3,depotend,startsol,seed+j,60*60,ou)

ou = "allsphfinalfixmake.txt"
for i,v in enumerate(outs2[3:]):
    for j in range(1): #repeat dif seeds
        helperclasses.appendToFile(ou,v+"\n")
        test(testfiles2,nrtasks,limits,True,sampling[i],samplingrange,fullrecharge,3,depotend,startsol,seed+j,60*60,ou)

#startsol = True

'''
testfiles = ['randinst_15_10test.txt','randinst_20_10test.txt']
for i,v in enumerate(outs+outs3):
    for j in range(3): #repeat dif seeds
        if (i+1)% len(outs)==0:
            startsol = True
        helperclasses.appendToFile("allsphtests.txt",v+"\n")
        test(testfiles,nrtasks,limits,obj[(i%len(outs))],sampling[(i%len(outs))],samplingrange,fullrecharge,3,depotend,startsol,seed+j,60*60,"allsphtests.txt")
'''
'''
testfiles2 = ["randinst_10_5test.txt"]
test(testfiles2,nrtasks,limits,True,[500],samplingrange,fullrecharge,3,False,False,seed,60*10,"tempout.txt")
'''



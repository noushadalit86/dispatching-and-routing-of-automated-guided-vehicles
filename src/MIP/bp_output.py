import helperclasses as helpc

'''
For outputting solutions or debugging


'''


def print_SPP(model, params):


    return

def print_MP(model, params):


    return


def solToSim(solution,problem):
    machines = []
    for i,v in enumerate(solution):
        indices = []
        setups = []
        sched = []
        ptimes = []
        starts = []
        cons = []
        prod = []
        for j in v:
            sched.append(j['index'])
            setups.append(j['setup'])
            ptimes.append(j['ptime'])
            starts.append(j['start'])
            
        machines.append({'index':i,'setups':setups,
                        'tasks':sched,'ptimes':ptimes,
                         'cons':[],'prod':[],'starts':starts})
    o = -1
    runtime = 0
    if problem.master.getAttr("SolCount")  >0:
        o=round(problem.master.getAttr("ObjVal"),4)
        runtime =problem.master.getAttr("Runtime")
    simsol = helpc.Results(machines,o, runtime, o, problem.master.getAttr("ObjBound"))
    return simsol


def print_sol(sol,fname):

    s = ""
    tot = 0
    for i,w in enumerate(sol):
        s+=str(i)
        for j,v in enumerate(w):
            s+="-"*(j==0)
            s+= "[i:{},s:{},p:{}]".format(v['index'],v['setup'],v['ptime'])
        if len(w)>0:
            c = v['start']+v['setup']+v['ptime']
        else:
            c = 0
        tot += c
        s += "c:{}\n".format(c)


    s+= "total: {}".format(tot)
    helpc.appendToFile(fname,s)
    return

def tikz_sol(sol,params,fname):
    
    s = "\\begin{tikzpicture}\n"
    n = params.instance.n
    m = len(sol)
    height = len(sol)+1
    width = 0
    s = s + "\\foreach \\r in {0,...,"+str(m-1)+"}\n"
    s = s + "   \draw (0,\\r) node[inner sep=1pt,below=3pt,rectangle,fill=white, left=2pt] {$\\r$};\n"
    s = s + "\draw[thick,->,black] (0,-1)--(0,"+str(m)+") node[left] {$vehicles$};\n"
    y = 0
    for i,v in enumerate(sol):
        x = 0
        tindex = 0
        for j,w in enumerate(v):
                label = str(w['index']-1)
                color = "rgb:red,2;green,5;yellow,0"
                if w['index']-1 >= n:
                    label = "r"
                    color = "rgb:red,2;green,5;yellow,8"
                #Check for idle time
                #arrival = (x*10+w['setup'])
                    '''
                if st[j] > arrival:
                    idle = st[j]-arrival
                    x = x + idle*0.1
                idle =   w['start']*0.1-x:
                '''
                x = w['start']*0.1
                #setup time
                oldx = x - w['setup']*0.1
                x = x 
                #s = s + "\draw("+str(oldx)+","+str(y)+") coordinate (A) -- ("+str(x)+","+str(y)+") coordinate (B);\n"
                s = s + "\draw[fill=gray] ("+str(oldx)+","+str(y-0.20)+") rectangle ("+str(x)+","+str(y+0.20)+") "
                s = s + "node[below=5pt,left=12.5pt, right=0, text width=5em] {$ $};\n"
                oldx = x
                x = x + w['ptime']*0.1
                s = s + "\draw[fill={"+color+"}] ("+str(oldx)+","+str(y-0.20)+") rectangle ("+str(x)+","+str(y+0.20)+") "
                s = s + "node[below=5pt,left=12.5pt, right=0, text width=5em] {$"+label+"$};\n"
                #x = x + (i['ptimes'][tindex])
        y = y+1
        if x > width:
                width = x
    s = s + "\draw[thick,->,black] (0,-1)--("+str(width)+",-1) node[left, below=10pt] {$time$};\n"
    s = s + "\\foreach \\r in {0, 5,...,"+str(width*10)+"}\n"
    s = s + "   \draw (\\r*0.1,-1) node[inner sep=1pt,below=5pt,rectangle,fill=white] {$\\r$};\n"
    s = s + "\end{tikzpicture}"
    helpc.appendToFile(fname,s)
    return

def testoutputheader(fname,makespan,sampling,offsetting):
    s = "\\begin{landscape}\n"
    s+="\\begin{table}[H]\n"
    s+="\\centering\n"
    s+="\\caption{Table showing the results from running the heuristic set partitioning model on instances with varying sets of columns. "
    if makespan:
        s+= "These runs used a makespan objective function. "
    else:
        s+= "These runs used a total time objective function. "
    if sampling:
        s+= "The schedule generation phase was done using sampling. "
    else:
        s+= "The schedule generation phase was done using a tasklimit. "
    if offsetting:
        s+= "Additionally, offsetting was applied to some of the schedules.}\n"
    else:
        s+="}\n"
    s+="\label{sphperf}\n"
    s+="\\begin{tabular}{|l|l|l|l|l|l|l|l|l|}\n"
    s+="\hline\n"
    #vehicles tasks nrcolumns tasklimit objective iterations time
    s+="Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\\ \hline \n"
    helpc.appendToFile(fname,s)
    return
def testoutputline(test,fname):
    helpc.appendToFile(fname,test.getLine()+"\\\ \hline \n")
    return

def testoutputfooter(fname):
    s="\end{tabular}\n"
    s+="\end{table}\n"
    s+="\end{landscape}\n"
            


    helpc.appendToFile(fname,s)

    return

def testoutput(test,fname):
    s = "\begin{landscape}\n"
    s+="\begin{table}[H]\n"
    s+="\centering\n"
    s+="\caption{Table showing the results from running the heuristic set partitioning model on instances with varying sets of columns.}\n"
    s+="\label{mipperf}\n"
    s+="\begin{tabular}{|l|l|l|l|l|l|l|l|}\n"
    s+="\hline\n"
    #vehicles tasks nrcolumns tasklimit objective iterations time
    s+="Vehicles & Tasks & \#Cols & Tasklimits & Objective & Iterations & Solve Time & Column Generation Time\\\ \hline \n"
    for i in test:
        s += i.getLine()
        s+="\\\ \hline \n"
    s+="\end{tabular}\n"
    s+="\end{table}\n"
    s+="\end{landscape}\n"
            


    helpc.appendToFile(fname,s)

    return

/* 
 * File:   iTask.h
 * Author: thor
 *
 * Created on February 28, 2016, 11:45 AM
 */

#ifndef ITASK_H
#define	ITASK_H

#include <string>
#include <vector>
#include <iostream>

using namespace std;

/* Representation of an instatnce of a task, with input parameters
 */
class ITask 
{
private: 
    int id;
    int minStartTime; // release date of job
    int duration;
    int deadline;
    int batteryCost;
    char taskType; // 'n' = normal, 'b' = battery
    vector <int> setupTimes; // setuptime from this task to all other tasks
    int setupToDepot;
    int setupFromDepot;
    
    static int idCounter;
    
public:  
    int getId(){ return id; }
    int getMinStartTime(){ return minStartTime; }
    int getDuration(){ return duration; }
    int getDeadline(){ return deadline; }
    int getBatteryCost(){ return batteryCost; }
    char getTaskType(){ return taskType; }
    int getSetupTime(int index){ return setupTimes.at(index); }
    int getSetupFromDepot() { return setupFromDepot; }
    int getSetupToDepot() { return setupToDepot; }
    vector<int> getSetupTimes() { return setupTimes; }

    void setMinStartTime(int time = 0);
    void setDeadline(int time);
    void setBatteryCost(int cost);
    void setDuration(int time);
    void setCharType(char type = 'n');
    void setSetupTime(int index, int value);
    void setId(int id, int value);
    void resizeSetupTime(int size);
    void setSetupToDepot(int setupTime);
    void setSetupFromDepot(int setupTime);
    void printTask(ostream&);
    
    ITask();
    // min start, duration, deadline, battery cost, task type
    ITask(int, int, int, int, int, char = 'n');
    
    ITask(const ITask& orig);
    virtual ~ITask();
};

#endif	/* ITASK_H */


/* 
 * File:   iVehicle.h
 * Author: thor
 *
 * Created on February 28, 2016, 11:44 AM
 */

#ifndef IVEHICLE_H
#define	IVEHICLE_H

#include <vector>
#include <iostream>

using namespace std;

class IVehicle {
private:
    int id;
    int minStart;
    int initialBattery;
    int minBattery;
    int maxBattery;

    static int idCounter;

 public:
    int getId(){ return id; }
    int getMinStart(){ return minStart; }
    int getInitialBattery(){ return initialBattery; }
    int getMinBattery(){ return minBattery; }
    int getMaxBattery(){ return maxBattery; }

    void setMinStart(int);
    void setInitialBattery(int);
    void setMinBattery(int);
    void setMaxBattery(int);

    IVehicle();
    // min start, initial battery, min battery, max battery
    IVehicle(int, int, int, int);
    IVehicle(const IVehicle& orig);
    virtual ~IVehicle();
};

#endif	/* IVEHICLE_H */


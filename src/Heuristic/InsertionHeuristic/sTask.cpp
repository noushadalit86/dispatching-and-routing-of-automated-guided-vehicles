/* 
 * File:   sTask.cpp
 * Author: thor
 * 
 * Created on February 28, 2016, 11:49 AM
 */

#include "sTask.h"


int STask::idCounter = 0;

/**
 * Sets everything except id to 0
 */
STask::STask() 
{
    this->id = idCounter++;
    this->startTime = 0;
    this->duration = 0;
    this->deadline = 0;
    this->endTime = 0;
    this->batterycost = 0;
    this->successorId = 0;
    this->successorIndex = -3;
    this->vehicleId = 0;
    this->setupTime = 0;
    this->batteryLevelAfterExecution = 0;
    this->arrivalTime = 0;
    this->setupFromDepot = 0;
    this->setupFromDepot = 0;
    this->arrivalTime = 0;
}

// start, duration, endtime, setuptime, batterycost, vehicleId, successorId
STask::STask(int start, int duration, int deadline, int end, int setup, 
        int batterycost, int vehicleId, int successorId)
{
    this->id = idCounter++;
    this->startTime = start;
    this->duration = duration;
    this->deadline = deadline;
    this->endTime = end;
    this->setupTime = setup;
    this->batterycost = batterycost;
    this->vehicleId = vehicleId;
    this->successorId = successorId;
    this->successorIndex = -3;
    this->arrivalTime = 0;
    this->batteryLevelAfterExecution = 0;
    this->setupFromDepot = 0;
    this->setupToDepot = 0;
    this->arrivalTime = 0;
}
STask::STask(const STask& orig) {
    this->id = orig.id;
    this->startTime = orig.startTime;
    this->duration = orig.duration;
    this->deadline = orig.deadline;
    this->endTime = orig.endTime;
    this->setupTime = orig.setupTime;
    this->batterycost = orig.batterycost;
    this->vehicleId = orig.vehicleId;
    this->successorId = orig.successorId;
    this->successorIndex = orig.successorIndex;
    this->arrivalTime = orig.arrivalTime;
    this->allSetupTimes = orig.allSetupTimes;
    this->batteryLevelAfterExecution = orig.batteryLevelAfterExecution;
    this->setupFromDepot = orig.setupFromDepot;
    this->setupToDepot = orig.setupToDepot;
    this->arrivalTime = orig.arrivalTime;
}

STask::STask(ITask iTask) {
    this->id = iTask.getId();
    this->startTime = 0;
    this->duration = iTask.getDuration();
    this->deadline = iTask.getDeadline();
    this->endTime = 0;
    this->setupTime = 0;
    this->allSetupTimes = iTask.getSetupTimes();
    this->batterycost = iTask.getBatteryCost();
    this->vehicleId = 0;
    this->successorId = 0;
    this->successorIndex = -3;
    this->arrivalTime = iTask.getMinStartTime();
    this->batteryLevelAfterExecution = 0;
    this->setupFromDepot = iTask.getSetupFromDepot();
    this->setupToDepot = iTask.getSetupToDepot();
}
STask::~STask() {
}

void STask::setStartTime(int time) 
{
    this->startTime = time;
}

void STask::setDuration(int time)
{
    this->duration = time;
}

void STask::setEndTime(int time) {
    this->endTime = time;
}

void STask::setSetupTime(int time)
{
    this->setupTime = time;
}

void STask::setVehicleId(int id)
{
    this->vehicleId = id;
}

void STask::setSuccessorId(int id) 
{
    this->successorId = id;
}

void STask::setId(int id) {
    this->id = id;
}

void STask::setBatterycost(int batteryConsumption) {
    this->batterycost = batteryConsumption;
}

void STask::setArrivalTime(int time) {
    this->arrivalTime = time;
}

void STask::setSetupTimes(int index, int value) {
    this->allSetupTimes[index] = value;
}

void STask::setBatteryLevelafterExecution(int level) {
    this->batteryLevelAfterExecution = level;
}

void STask::setDeadline(int time) {
    this->deadline = time;
}

void STask::printTask(ostream& os) {
    os << this->id << "\t";
    os << this->startTime << "\t";
    os << this->duration << "\t";
    os << this->deadline << "\t";
    os << this->endTime << "\t";
    os << this->setupTime << "\t";
    os << this->batterycost << "\t";
    os << this->batteryLevelAfterExecution << "\t";
    os << this->successorIndex << "\t";
    os << this->setupFromDepot << "\t";
    os << this->setupToDepot << "\t";
    os << this->successorId << endl;
}

void STask::printTaskHeader(ostream& os) {
    os << "--id\tstart\tdura\tdeadl\tend\tsetup\tbatCost"
                "\tbatLvl\tnIndex\tFromD\tToD\tnextId" << endl;
}

void STask::printAllSetupTimes(ostream& os) {
    for (int i = 0; i < allSetupTimes.size(); i++) {
        os << allSetupTimes[i] << ", ";
    }
    os << endl;
}

void STask::setSuccessorIndex(int index) {
    this->successorIndex = index;
}

void STask::setSetupFromDepot(int setup) {
    this->setupFromDepot = setup;
}

void STask::setSetupToDepot(int setup) {
    this->setupToDepot = setup;
}

int STask::getSetupTimeTo(int index) {
    if (index == -1 || index == -2 || index == -4) {
        return setupToDepot;
    } else {
        return allSetupTimes[index];
    }
}

void STask::setSetupTimes(vector<int> allSetupTimes) {
    this->allSetupTimes = allSetupTimes;
}
/* 
 * File:   solution.cpp
 * Author: thor
 * 
 * Created on February 28, 2016, 11:49 AM
 */

#include "solution.h"

Solution::Solution() {
    feasible = true;
}

Solution::Solution(Instance ins) {
    goToDepotAtEnd = ins.getGoToDoDepotAtEnd();
    feasible = true;
    // solution tasks
    tasks = vector<STask>(ins.getTasks().size());
    
    for (int i = 0; i < ins.getTasks().size(); i++) {
        tasks.at(i) = STask(ins.getTask(i));         
    }
    STask start; // everything is 0 by default except id
    start.setId(-1); // -1 is start task
    start.setSuccessorId(-2); // -2 is end task
    start.setDeadline(ins.getUpperbound());
    start.setSetupFromDepot(0);
    start.setSetupToDepot(0);
    start.setSetupTimes(ins.getSetupTimesForTask(0));
    STask end;
    end.setId(-2); // -2 is end task // -4 is battery recharge
    end.setSuccessorId(-3); // no task
    end.setDeadline(ins.getUpperbound());
    end.setSetupFromDepot(0);
    end.setSetupToDepot(0);
    end.setSetupTimes(ins.getSetupTimesForTask(ins.getNumTasks()+1));
    
    vehicles = vector<SVehicle>(ins.getVehicles().size());
    
    // solution vehicles
    for (int i = 0; i < ins.getVehicles().size(); i++) {
        vehicles[i].setId(ins.getVehicle(i).getId());
        vehicles[i].setInitBattery(ins.getVehicle(i).getInitialBattery());
        STask startTask = start;
        startTask.setStartTime(ins.getVehicle(i).getMinStart());
        startTask.setEndTime(ins.getVehicle(i).getMinStart());
        startTask.setBatteryLevelafterExecution(ins.getVehicle(i).getInitialBattery());
        vehicles[i].setStartTask(startTask);
        vehicles[i].setGoToDepotAtEnd(ins.getGoToDoDepotAtEnd());
        STask endTask = end;
        endTask.setStartTime(ins.getVehicle(i).getMinStart());
        endTask.setEndTime(ins.getVehicle(i).getMinStart());
        endTask.setBatteryLevelafterExecution(ins.getVehicle(i).getInitialBattery());
        vehicles[i].setEndTask(endTask);
        vehicles[i].setMinBattery(ins.getVehicle(i).getMinBattery());
        vehicles[i].setMaxBattery(ins.getVehicle(i).getMaxBattery());
    }
    
    makespan = 0;
}

Solution::Solution(const Solution& orig) {
    this->tasks = orig.tasks;
    this->vehicles = orig.vehicles;
    this->makespan = orig.makespan;
    this->elapsedTime = orig.elapsedTime;
    this->feasible = orig.feasible;
    this->goToDepotAtEnd = orig.goToDepotAtEnd;
}

Solution::~Solution() {
}

void Solution::setMakespan(int makespan) {
    this->makespan = makespan;
}

void Solution::setTask(int index, STask sTask) {
    this->tasks.at(index) = sTask;
}

void Solution::setVehicle(int index, SVehicle sVehicle) {
    this->vehicles.at(index) = sVehicle;
}

void Solution::printSolution(ostream& os) {
    os << "--Printing solution:" << endl;
    for (int i = 0; i < vehicles.size(); i++) {
        os << "--vehicle: " << i << ", num tasks: " 
                << vehicles[i].getTasks().size()+2 << endl;
        STask task = vehicles[i].getStartTask();
        task.printTaskHeader(os);
        STask end = vehicles[i].getEndTask();
        task.printTask(os);
        for (int j = 0; j < vehicles[i].getTasks().size(); j++) {
            vehicles[i].getTask(j).printTask(os);
        }
        end.printTask(os);
    }
}

void Solution::outputToTikz(ostream& os) {
    int numVehicles = vehicles.size();
    int numTasks = tasks.size();
    double width = 0;
    os << "\\foreach \\r in {0,...," << numVehicles-1 << "}\n";
    os << "   \\draw (0,\\r) node[inner sep=1pt,below=3pt,rectangle,fill=white, left=2pt] {$\\r$};\n";
    os << "\\draw[thick,->,black] (0,-1)--(0," << numVehicles << ") node[left] {$vehicles$};\n";
    double y = 0;
    for (int i = 0; i < numVehicles; i++) {
        SVehicle v = vehicles[i];
        double previousSetup = v.getStartTask().getSetupTimeAsDouble();
        double x = 0;
        for (int j = 0; j < v.getTasks().size(); j++) {
            STask t = v.getTask(j);
            const char* color = "rgb:red,2;green,5;yellow,0";
            const char* label = " ";
            // Label
            string s = patch::to_string(t.getId());
            label = s.c_str();
            if (t.getId() == -4) {
                // Task is a recharge
                label = "R"; 
                color = "rgb:red,2;green,5;yellow,8";
            }
            double start = t.getStartTimeAsDouble()-previousSetup;
            // Idle time
            double arrival = start*0.1;
            if (x-arrival < 0) {
                x = x - (x-arrival);
            } 
            // Setup Time
            double oldx = x;
            if (previousSetup > 0) {
                x = x + previousSetup*0.1;
                os << "\\draw[fill=gray] (" << oldx << "," << y-0.20 << ") rectangle (" << x << "," << y+0.20 << ") ";
                os << "node[below=5pt,left=12.5pt, right=0, text width=5em] {$ $};\n";
            }
            oldx = x;
            x = x + t.getDurationAsDouble()*0.1;
            os << "\\draw[fill={" << color << "}] (" << oldx << "," << y-0.20 << ") rectangle (" << x << "," << y+0.20 << ") ";
            os << "node[below=5pt,left=12.5pt, right=0, text width=5em] {$" << label << "$};\n";
            previousSetup = t.getSetupTimeAsDouble();
        }
        y = y+1;
        if (x > width) {
            width = x;
        }
    }
    os << "\\draw[thick,->,black] (0,-1)--(" << width << ",-1) node[left, below=10pt] {$time$};\n";
    os << "\\foreach \\r in {0, 5,...," << width*10 <<"}\n";
    os << "\\draw (\\r*0.1,-1) node[inner sep=1pt,below=5pt,rectangle,fill=white] {$\\r$};\n";
}

void Solution::outputSolution(ostream& os) {
    int numVehicles = vehicles.size();
    os << elapsedTime << endl;
    os << makespan << endl;
    for (int j = 0; j < numVehicles; j++) {
        SVehicle v = vehicles[j];
        int vSize = v.getTasks().size();
        os << "-" << endl;
        // task id
        for (int i = 0; i < vSize; i++) {
            int taskId = v.getTask(i).getId();
            if (v.getTask(i).getId() == -4) {
                taskId = -1;
            }
            os << taskId << " ";
        }
        if (vSize > 0 && v.getTask(vSize-1).getId() != -4) {
            os << "-1";
        }
        os << endl;
        // task duration
        for (int i = 0; i < vSize; i++) {
            os << v.getTask(i).getDuration() << " ";
        }
        if (vSize > 0 && v.getTask(vSize-1).getId() != -4) {
            os << "0";
        }
        os << endl;
        // task setup time
        for (int i = 0; i < vSize; i++) {
            if (i == 0) {
                os << v.getTask(i).getSetupFromDepot() << " ";
            } else {
                os << v.getTask(i-1).getSetupTime() << " ";
            }
        }
        if (vSize > 0 && v.getTask(vSize-1).getId() != -4) {
            if (shouldGoToDepotAtEnd()) {
                os << v.getTask(vSize-1).getSetupToDepot();     
            } else {
                os << 0;
            }
            
        }
        os << endl;
        // task start time
        for (int i = 0; i < vSize; i++) {
            os << v.getTask(i).getStartTime() << " ";
        }
        if (vSize > 0 && v.getTask(vSize-1).getId() != -4) {
            if (shouldGoToDepotAtEnd()) {
                os << v.getTask(vSize-1).getEndTime()+v.getTask(vSize-1).getSetupToDepot();
            } else {
                os << v.getTask(vSize-1).getEndTime();
            }
            
        }
        os << endl;
    }
}

void Solution::setElapsedTime(double time) {
    elapsedTime = time;
}

void Solution::setFeasibility(bool b) {
    feasible = b;
}

void Solution::setGoToDepotAtEnd(int toGoDepotAtEnd) {
    this->goToDepotAtEnd = goToDepotAtEnd;
}
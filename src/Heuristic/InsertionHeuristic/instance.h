/* 
 * File:   instance.h
 * Author: thor
 *
 * Created on February 28, 2016, 11:49 AM
 */

#ifndef INSTANCE_H
#define	INSTANCE_H

#include <vector>
#include <ostream>
#include <iostream>

#include "iTask.h"
#include "iVehicle.h"

typedef vector<int> Row;
typedef vector<Row> Matrix;

using namespace std;

class Instance {    
private:
    int numTasks;
    int numVehicles;
    int lowerbound;
    int upperbound;
    int numBatteryResources;
    vector <ITask> tasks;
    vector <IVehicle> vehicles;
    Matrix setupTimes;
    int maxRechargeAmount;
    int rechargeTimeToMax;
    int rechargePerTimeUnit;
    int goToDepotAtEnd;
    
public:    
    int getNumTasks(){ return numTasks; }
    int getNumVehicles(){ return numVehicles; }
    int getLowedbound(){ return lowerbound; }
    int getUpperbound(){ return upperbound; }
    ITask& getTask(int index){ return tasks.at(index); }
    IVehicle& getVehicle(int index){ return vehicles.at(index); }
    int getSetuptime(int i, int j){ return setupTimes[i][j]; }
    int getMaxRechargeAmount(){ return maxRechargeAmount; }
    int getRechargeTimeToMax(){ return rechargeTimeToMax; }
    int getRechargePerTimeUnit(){ return rechargePerTimeUnit; }
    vector<ITask>& getTasks() { return tasks; }
    vector<IVehicle>& getVehicles() { return vehicles; }
    vector<int> getSetupTimesForTask(int index) { return setupTimes[index]; }
    int getSetupFromDepotToTask(int index) { return setupTimes[0][index]; }
    int getNumBatteryResources() { return numBatteryResources; }
    int getGoToDoDepotAtEnd() { return goToDepotAtEnd; }
    
    void setNumTasks(int);
    void setNumVehicles(int);
    void setLowerbound(int);
    void setUpperbound(int);
    void addTask(ITask);
    void addVehicle(IVehicle);
    void setTask(int, ITask);
    void setVehicle(int, IVehicle);
    void resizeSetuptime(int i, int j);
    void setSetuptime(int i, int j, int value);
    void setMaxRechargeAmount(int amount);
    void setRechargeTimeToMax(int time);
    void setRechargePerTimeUnit(int amount);
    void printInstance(ostream& os);
    void printSetupTimes(ostream& os);
    void setNumBatteryResources(int);
    void setGoToDepotAtEnd(int);
    
    
    Instance();
    Instance(const Instance& orig);
    virtual ~Instance();
};

#endif	/* INSTANCE_H */


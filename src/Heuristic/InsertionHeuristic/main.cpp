/* 
 * File:   main.cpp
 * Author: thor
 *
 * Created on February 25, 2016, 10:53 AM
 */

#include "main.h"

using namespace std;
string input = "";
char* output;
int timelimit = 0;
int batteryResources = 3;
int routeCriteria = 0;
int insertionCriteria = 0;
int taskCriteria = 0;
int initCriteria = 0;
double initAlpha1 = 0.5;
double initAlpha2 = 0.5;
double mu = 0.5;
double alpha1 = 0.3;
double alpha2 = 0.5;
double alpha3 = 0.2;
double lambda = 0.5;
double beta1 = 0.5;
double beta2 = 0.5;
int rechargeType = 0;
int goToDepotAtEnd = 0;
int randomSeed = 1337;
char* tikzOutput;
Instance ins;

/* Example
 "${OUTPUT_PATH}" 
 instances/randinst_10_5_fixed.txt 
 results/result.txt
 1 // initial task criteria
 1 // route selection criteria
 1 // insertion criteria
 1 // task selection criteria
 0 // timelimit 0 = unlimited
 3 // battery resources available
 results/tikz.txt
 0.5 // init alpha 1
 0.5 // init alpha 2
 0.5 // mu
 0.5 // lambda
 0.3 // alpha 1
 0.5 // alpha 2
 0.2 // alpha 3
 0.5 // beta 1
 0.5 // beta 2
 0 // recharge type (0 = variable, 1 = fixed)
 0 // 0 = no, 1 = yes.
 1337 // random seed
 */
       
/// Reads an instance from a text file (.txt)
void parseInputToInstance(string inputFile) 
{
    cout << "parsing inputfile: " << inputFile << endl;
    
    // read input to vector
    ifstream reader(inputFile.c_str());
    
    vector<int> input;
    if (!reader) {
        cout << "Error opening file" << endl;
        throw("Error opening file: " + inputFile);
    } else {
        string word;        
        while (reader >> word) {
            if (word.find("-")==string::npos) {
                input.push_back(atoi(word.c_str()));
            }
        }
        reader.close();
    }
    int index = 0;
    ins.setNumTasks(input[index++]); // num tasks
    ins.setNumVehicles(input[index++]); // num vehicles

    // fill in durations
    index++; // duration (first is dummy)
    for (int i = 0; i < ins.getNumTasks(); i++) {
        ins.getTask(i).setDuration(input[index++]);
    };
    index++; // duration (last is dummy)
    ins.setUpperbound(input[index++]); // set upper bound
    index++; // start time (first is dummy)
    // fill in start times
    for (int i = 0; i < ins.getNumTasks(); i++) {
        ins.getTask(i).setMinStartTime(input[index++]);
    }
    index++; // start time (last is dummy)
    index++; // deadline (first dummy)
    for (int i = 0; i < ins.getNumTasks(); i++) {
        ins.getTask(i).setDeadline(input[index++]);
    }
    index++; // deadline (last is dummy)
    // setuptime
    
    for (int k = 0; k < ins.getNumTasks(); k++) {
        ins.getTask(k).resizeSetupTime(ins.getNumTasks());
    }
    ins.resizeSetuptime(ins.getNumTasks()+2, ins.getNumTasks()+2);
    for (int i = 0; i < ins.getNumTasks()+2; i++) {
        for (int j = 0; j < ins.getNumTasks()+2; j++) {
            ins.setSetuptime(i,j,input[index++]);
        }
    }
    // max battery for vehicles
    for (int i = 0; i < ins.getNumVehicles(); i++) {
        ins.getVehicle(i).setMaxBattery(input[index]);
    }
    int minBat = input[index];
    index++;
    // min battery for vehicles
    for (int i = 0; i < ins.getNumVehicles(); i++) {
        ins.getVehicle(i).setMinBattery(input[index]);
    }
    int maxBat = input[index];
    index++;
    
    int batSpan = maxBat-minBat;
    
    // charge time for charge tasks
    int chargeTime = input[index++];
    int chargeAmount = input[index++];
    ins.setRechargePerTimeUnit(
            chargeAmount/chargeTime);
    ins.setMaxRechargeAmount(batSpan);
    ins.setRechargeTimeToMax(batSpan/ins.getRechargePerTimeUnit());
    // initial battery of vehicles
    for (int i = 0; i < ins.getNumVehicles(); i++) {
        ins.getVehicle(i).setInitialBattery(input[index++]);
    }
    index++; // task battery cost (first is dummy)
    for (int i = 0; i < ins.getNumTasks(); i++) {
        ins.getTask(i).setBatteryCost(input[index++]);
    }
    index++; // task battery cost (last is dummy)
    // vehicle min start times
    if (input.size() > index) { // if so, then we also have min vehicle start times
        for (int i = 0; i < ins.getNumVehicles(); i++) {
            ins.getVehicle(i).setMinStart(input[index++]);
        }
    } else {
        for (int i = 0; i < ins.getNumVehicles(); i++) {
            ins.getVehicle(i).setMinStart(0);
        }
    }
}

/* Example
 "${OUTPUT_PATH}" 
 instances/randinst_10_5_fixed.txt 
 results/result.txt
 1 // initial task criteria
 1 // route selection criteria
 1 // insertion criteria
 1 // task selection criteria
 0 // timelimit 0 = unlimited
 3 // battery resources available
 results/tikz.txt
 0.5 // init alpha 1
 0.5 // init alpha 2
 0.5 // mu
 0.5 // lambda
 0.3 // alpha 1
 0.5 // alpha 2
 0.2 // alpha 3
 0.5 // beta 1
 0.5 // beta 2
 0 // recharge type (0 = variable, 1 = fixed)
 0 // 0 = no, 1 = yes.
 1337 // random seed
 */
        
int main(int argc, char** argv) {
    cout << "argc: " << argc << endl;
    // parse arguments
    // check if enough arguments
    if (argc < 21) {
        cout << "too few arguments, "
                "please specify: 'inputfile' "
                "'outputfile' "
                "'initialization criteria'"
                "'route selection criteria' "
                "'insertion place criteria' "
                "'task selection criteria"
                "'timelimit' "
                "'battery resource available' (3 default) " 
                "'tikz output' "
                "'initAlpha1' 'initAlpha2' 'mu' 'alpha1' 'alpha2' "
                "'alpha3' 'lambda' 'beta1' 'beta2' 'recharge: variable=0/max=1' 'detpotAtEnd: no=0/yes=1" << endl;
        return -1;
    } else {
        input = argv[1];
        output = argv[2];
        initCriteria = atoi(argv[3]);
        routeCriteria = atoi(argv[4]);
        insertionCriteria = atoi(argv[5]);
        taskCriteria = atoi(argv[6]);
        timelimit = atoi(argv[7]);
        batteryResources = atoi(argv[8]);
        tikzOutput = argv[9];
        initAlpha1 = atof(argv[10]);
        initAlpha2 = atof(argv[11]);
        mu = atof(argv[12]);
        lambda = atof(argv[13]);
        alpha1 = atof(argv[14]);
        alpha2 = atof(argv[15]);
        alpha3 = atof(argv[16]);
        beta1 = atof(argv[17]);
        beta2 = atof(argv[18]);
        rechargeType = atoi(argv[19]);
        goToDepotAtEnd = atoi(argv[20]);
        randomSeed = atoi(argv[21]);
    }

    
    // print arguments
    cout << "Number of arguments: " << argc << endl;
    cout << "execution: " << argv[0] << endl;
    cout << "intput: " << input << endl;
    cout << "output: " << output << endl;
    cout << "init criteria: " << initCriteria << endl;
    cout << "route criteria: " << routeCriteria << endl;
    cout << "insertion criteria " << insertionCriteria << endl;
    cout << "task criteria " << taskCriteria << endl;
    cout << "timelimit: " << timelimit << endl;
    cout << "battery resources: " << batteryResources << endl;
    cout << "tikz output: " << tikzOutput << endl;
    cout << "recharge type: " << rechargeType << endl;
    cout << "goToDepotAtEnd: " << goToDepotAtEnd << endl;
    cout << "random seed: " << randomSeed << endl;
    
    // parse input into "ins" Instance
    parseInputToInstance(input);
    ins.setNumBatteryResources(batteryResources);
    ins.setGoToDepotAtEnd(goToDepotAtEnd);
    
    Heuristic heu(ins);
    // params
    heu.setInsertionParams(initAlpha1, initAlpha2);
    heu.setMu(mu);
    heu.setAlphas(alpha1, alpha2, alpha3);
    heu.setLambda(lambda);
    heu.setBetas(beta1, beta2);
    // heuristic type uses 0 index
    heu.setInitHeuristic(initCriteria-1); // 0 - 3
    heu.setRouteHeuristic(routeCriteria-1); // 0 - 2
    heu.setInsertHeuristic(insertionCriteria-1); // 0 - 1
    heu.setTaskHeuristic(taskCriteria-1); // 0 - 2
    heu.setRechargeType(rechargeType);
    clock_t begin = clock();
    Solution sol = heu.runHeuristic();
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    sol.setElapsedTime(elapsed_secs);
    if (sol.isFeasible()) {
        ofstream o_tikz(tikzOutput,std::ofstream::binary);
        sol.outputToTikz(o_tikz);
        ofstream o_sol(output,std::ofstream::binary);
        sol.outputSolution(o_sol);
    } else {
        cout << "solution infeasible" << endl;
        return -1;
    }
    sol.printSolution(cout);
    sol.outputSolution(cout);
    return 0;
}


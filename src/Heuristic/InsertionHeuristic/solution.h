/* 
 * File:   solution.h
 * Author: thor
 *
 * Created on February 28, 2016, 11:49 AM
 */

#ifndef SOLUTION_H
#define	SOLUTION_H

#include <vector>
#include <string>
#include <stdlib.h>
#include <sstream>

#include "sTask.h"
#include "sVehicle.h"
#include "instance.h"
#include "iTask.h"
#include "iVehicle.h"



namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

using namespace std;

class Solution {
public:
    Solution();
    Solution(Instance);
    Solution(int);
    Solution(const Solution& orig);
    virtual ~Solution();
    void setTask(int, STask);
    void setVehicle(int, SVehicle);
    void setMakespan(int);
    void printSolution(ostream&);
    void outputSolution(ostream&);
    void outputToTikz(ostream&);
    void setElapsedTime(double time);
    bool isFeasible() { return feasible;}
    bool shouldGoToDepotAtEnd() { return (goToDepotAtEnd == 1); }
    
    vector<SVehicle>& getVehicles() { return vehicles; }
    SVehicle& getVehicle(int index) { return vehicles[index]; }
    vector<STask>& getTasks() { return tasks; }
    int getMakespan() { return makespan; }
    STask& getTask(int index) { return tasks[index]; }
    double getElapsedTime() { return elapsedTime; }
    void setFeasibility(bool);
    void setGoToDepotAtEnd(int);
private:
    int makespan;
    double elapsedTime;
    vector<STask> tasks;
    vector<SVehicle> vehicles;
    bool feasible;
    int goToDepotAtEnd;
};

#endif	/* SOLUTION_H */


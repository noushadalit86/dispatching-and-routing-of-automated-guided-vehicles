/* 
 * File:   solver.h
 * Author: thor
 *
 * Created on October 14, 2015, 12:54 PM
 */

#ifndef SOLVER_H
#define	SOLVER_H

#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <vector>       // std::vector
#include <stdlib.h>     // std::atoi

#include <cstdlib>
#include <string>
#include <ostream>

#include "model.h"      // gecode model
#include "instance.h"   // Instance of a problem
#include "solution.h"   // Repesentation of a solution


using namespace std;

Instance preprocess(const char* inputFile, 
        const char* secondOut, int timelimit, int num_battery_chargers, int goToDepotAtEnd);
Solution solve(Instance toSolve);

#endif	/* SOLVER_H */


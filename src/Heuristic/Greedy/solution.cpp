/* 
 * File:   Solution.cpp
 * Author: thor
 * 
 * Created on October 14, 2015, 1:47 PM
 */

#include "solution.h"
#include <stdlib.h>
#include <string>
#include <sstream>

namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

using namespace std;

Solution::Solution() {
    didFirstOut = false;
    sjsfout = "";
    sfout = "";
}
Solution::Solution(int* tasks, int* setuptimes, int* processingtimes) {
    didFirstOut = false;
    sjsfout = "";
    sfout = "";
}

void Solution::printSolution() {
    cout << "solution: " << endl;
    for (int i = 0; i < num_machines; i++) {
        cout << "vehicle " << i << ":\tId\tStart\tDur\tEnd\tSetup\tbattery" << endl;
        for (int j = 0; j < 2*num_tasks; j++) {
            
            if (vehicles[i].tasks[j].processingTime > -1) {
                cout << "\t\t"<< vehicles[i].tasks[j].label << "\t"
                        << vehicles[i].tasks[j].startTime << "\t"
                        << vehicles[i].tasks[j].processingTime << "\t"
                        << vehicles[i].tasks[j].endTime << "\t"
                        << vehicles[i].tasks[j].setupTime << "\t"
                        << vehicles[i].tasks[j].battery << endl;
            }
        }
    }
}

void Solution::outputTikz(ostream& os) {
    double width = 0;
    os << "\\foreach \\r in {0,...," << num_machines-1 << "}\n";
    os << "   \\draw (0,\\r) node[inner sep=1pt,below=3pt,rectangle,fill=white, left=2pt] {$\\r$};\n";
    os << "\\draw[thick,->,black] (0,-1)--(0," << num_machines << ") node[left] {$vehicles$};\n";
    double y = 0;
    for (int i = 0; i < num_machines; i++) {
        //cout << "vehicleId\ttaskId\tstart\tarrival\tsetup\tDuration"<< endl;
        double x = 0;
        for (int j = 0; j < 2*num_tasks; j++) {
            
            const char* color = "rgb:red,2;green,5;yellow,0";
            const char* label = " ";
            if (vehicles[i].tasks[j].processingTime > 0) {
                // Label
                string s = patch::to_string(vehicles[i].tasks[j].label);
                label = s.c_str();
                if (vehicles[i].tasks[j].label == -1) {
                    // Task is a recharge
                    label = "R"; 
                    color = "rgb:red,2;green,5;yellow,8";
                }
                // Idle time
                double arrival = vehicles[i].tasks[j].startTime*0.1;
                //x = x - arrival;
                double setupStart = arrival - vehicles[i].tasks[j].startTime*0.1;
                if (setupStart-x > 0) {
                    x = setupStart;
                }
                // Setup Time
                double oldx = x;
                x = x + vehicles[i].tasks[j].setupTime*0.1;
                os << "\\draw[fill=gray] (" << oldx << "," << y-0.20 << ") rectangle (" << x << "," << y+0.20 << ") ";
                os << "node[below=5pt,left=12.5pt, right=0, text width=5em] {$ $};\n";
                x = x + vehicles[i].tasks[j].processingTime*0.1;
                os << "\\draw[fill={" << color << "}] (" << oldx << "," << y-0.20 << ") rectangle (" << x << "," << y+0.20 << ") ";
                os << "node[below=5pt,left=12.5pt, right=0, text width=5em] {$" << label << "$};\n";
            }
        }
        y = y+1;
        if (x > width) {
            width = x;
        }
    }
    os << "\\draw[thick,->,black] (0,-1)--(" << width << ",-1) node[left, below=10pt] {$time$};\n";
    os << "\\foreach \\r in {0, 5,...," << width*10 <<"}\n";
    os << "\\draw (\\r*0.1,-1) node[inner sep=1pt,below=5pt,rectangle,fill=white] {$\\r$};\n";
}

void Solution::outputFirst() {
    if (!didFirstOut) {
        cout << "should give a solution here" << endl;
        ofstream fout(sfout,std::ofstream::binary);
        didFirstOut = true;
        fout << elapsed_time << endl;
        fout << makespan << endl;
        int index = 0;
        for (int j = 0; j < num_machines; j++) {
            fout << "-" << endl;
            for (int i = index; i-index < tasksPerVehicle[j]; i++) {
                fout << tasks[i] << " ";
            }
            fout << endl;
            for (int i = index; i-index < tasksPerVehicle[j]; i++) {
                fout << processingtimes[i] << " ";
            }
            fout << endl;
            for (int i = index; i-index < tasksPerVehicle[j]; i++) {
                fout << setuptimes[i] << " ";
            }
            fout << endl;
            index += tasksPerVehicle[j];
        }
    }
}
void Solution::outputSolution(ostream& os) {
    cout << endl;
    int ms = 0;
    // fix makespan representation
    for (int i = 0; i < num_machines; i++) {
        for (int j = 0; j < 2*num_tasks; j++) {
            if (vehicles[i].tasks[j].processingTime > 0) {
                if (vehicles[i].tasks[j].label != -1 && goToDepotAtEnd == 1) {
                    if (ms < vehicles[i].tasks[j].endTime + vehicles[i].tasks[j].setupToDepot) {
                        ms = vehicles[i].tasks[j].endTime + vehicles[i].tasks[j].setupToDepot;
                    }
                } else {
                    if (ms < vehicles[i].tasks[j].endTime) {
                        ms = vehicles[i].tasks[j].endTime;
                    }     
                }
                
            }
        }
    }

    os << elapsed_time << endl;
    os << ms << endl;
    for (int i = 0; i < num_machines; i++) {
        os << "-" << endl;
        // output label
        int lastIndex = 0;
        for (int j = 0; j < 2*num_tasks; j++) {
            if (vehicles[i].tasks[j].processingTime > 0) {
                os << vehicles[i].tasks[j].label << " ";
                lastIndex = j;
            }
        }
        if (goToDepotAtEnd == 1 && vehicles[i].tasks[lastIndex].label != -1) {
            os << "-1";
        } else if (vehicles[i].tasks[lastIndex].label != -1) {
            os << "-1";
        }
        os << endl;
        // output processing time
        for (int j = 0; j < 2*num_tasks; j++) {
            if (vehicles[i].tasks[j].processingTime > 0) {
                os << vehicles[i].tasks[j].processingTime << " ";    
            }
        }
        if (vehicles[i].tasks[lastIndex].label != -1) {
            os << "0";
        }
        os << endl;
        // output setuptimes
        for (int j = 0; j < 2*num_tasks; j++) {
            if (vehicles[i].tasks[j].processingTime > 0) {
                os << vehicles[i].tasks[j].setupTime << " ";
            }
        }
        if (goToDepotAtEnd == 1 && vehicles[i].tasks[lastIndex].label != -1) {
            os << vehicles[i].tasks[lastIndex].setupToDepot;
        } else if (vehicles[i].tasks[lastIndex].label != -1) {
            os << "0";
        }
        os << endl;
        for (int j = 0; j < 2*num_tasks; j++) {
            if (vehicles[i].tasks[j].processingTime > 0) {
                os << vehicles[i].tasks[j].startTime << " ";
            }
        }
        if (goToDepotAtEnd == 1 && vehicles[i].tasks[lastIndex].label != -1) {
            os << vehicles[i].tasks[lastIndex].setupToDepot+vehicles[i].tasks[lastIndex].endTime;
        } else if (vehicles[i].tasks[lastIndex].label != -1) {
            os << vehicles[i].tasks[lastIndex].endTime;
        }
        os << endl;
    }
}



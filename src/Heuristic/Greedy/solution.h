/* 
 * File:   Solution.h
 * Author: thor
 *
 * Created on October 14, 2015, 1:47 PM
 */

#ifndef SOLUTION_H
#define	SOLUTION_H

#include <fstream>      // std::ifstream
#include <iostream>     // std::cout
#include <ostream>
#include <string>


using namespace std;

struct Task {
    int processingTime;
    int setupTime;
    int label;
    int startTime;
    int battery;
    int endTime;
    int setupToDepot;
};

struct Vehicle
{
    Task* tasks;
};

class Solution {
    
public:
    int makespan;
    int num_machines;
    int num_tasks;
    int maxBatteryCapacity;
    int* tasks;
    int* setuptimes;
    int* processingtimes;
    int* tasksPerVehicle;
    int* starttimes;
    int goToDepotAtEnd;
    double elapsed_time;
    bool didFirstOut;
    const char* sfout;
    const char* sjsfout;
    Solution();
    Solution(int* tasks, int* setuptimes, int* processingtimes);
    void outputFirst();
    void printSolution();
    void outputSolution(ostream& os);
    void outputTikz(ostream& os);
    Vehicle* vehicles;
private:

};

#endif	/* SOLUTION_H */


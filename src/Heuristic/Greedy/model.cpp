

#include "model.h"
#include "instance.h"
#include "solution.h"

#include <fstream>
#include <iostream>
#include <string>


using namespace std;

static Instance ins;
static Solution sol;

Model::Model() {
    int n = num_tasks();
    int m = num_machines();
    
    timelimit = ins.getTimelimit();
    numScheduledTasks = 0;
    vehicleBattery = new int[m];
    vehicleBusyTime = new int[m];
    vehiclePredecessorTask = new int[m];
    vehiclePredecessorTaskBattery = new int[m];
    vehicleTaskNum = new int[m];
    vehicleHasFullBattery = new int[m];
    for (int i = 0; i < m; i++) {
        vehicleBusyTime[i] = ins.getVehicleStartTimes()[i];
        vehiclePredecessorTask[i] = -1;
        vehiclePredecessorTaskBattery[i] = -1;
        vehicleBattery[i] = ins.getInitialBattery()[i];
        vehicleTaskNum[i] = 0;
        vehicleHasFullBattery[i] = 1;
    }
    
    taskScheduled = new bool[n];
    for (int i = 0; i < n; i++) {
        taskScheduled[i] = false;
    }
    
    // create the vehicle and task structure.
    sol.vehicles = new Vehicle[m];
    sol.maxBatteryCapacity = ins.getMinMaxBattery()[1];
    for (int i = 0; i < m; i++) {
        sol.vehicles[i].tasks = new Task[2*n];
        for (int j = 0; j < 2*n; j++) {
            sol.vehicles[i].tasks[j].processingTime = -1;
            sol.vehicles[i].tasks[j].label = -1;
            sol.vehicles[i].tasks[j].setupTime = -1;
            sol.vehicles[i].tasks[j].startTime = -1;
            sol.vehicles[i].tasks[j].battery = -1;
            sol.vehicles[i].tasks[j].endTime = -1;
            sol.vehicles[i].tasks[j].setupToDepot = -1;
        }
    }
    sol.goToDepotAtEnd = ins.getGoToDepotAtEnd();
    cout << "created model" << endl;
}

void Model::solve() {
    // initial solution stuff:
    int num_battery = 0;
    sol.num_machines = num_machines();
    sol.num_tasks = num_tasks();
    clock_t begin = clock();
    bool timelimit = false;
    int max = max_time();
    int earliestBatteryEndTime = max;
    vector<int> batteryRechargeEndTimes;
    // i = time
    // j = task num
    // k = machine num
    // go through all tasks
    for (int i = 0; i < max; i++) {
        // check if we are over out time limit, if so then return
        clock_t end = clock();
        if (((end-begin) / CLOCKS_PER_SEC) > timelimit) {
            return;
        }
        
        // All vehicles at full capacity
        int allVehiclesFull = 1;
        for (int l = 0; l < num_machines(); l++) {
            if (vehicleHasFullBattery[l] == 0) {
                allVehiclesFull = 0;
            }
        }
        
        // check if we scheduled all tasks
        if (numScheduledTasks == num_tasks() && ((allVehiclesFull && ins.getGoToDepotAtEnd() == 1) || ins.getGoToDepotAtEnd() == 0))  {
            for (int z = 0; z < num_machines(); z++) {
                if (sol.makespan < vehicleBusyTime[z]) {
                    sol.makespan = vehicleBusyTime[z];
                }
            }
            // since time starts at -1, we had 1 to makespan.
            sol.makespan++;
            return;
        }
        
        
        for (int l = 0; l < batteryRechargeEndTimes.size(); l++) {
            if (batteryRechargeEndTimes[l] <= i) {
                batteryRechargeEndTimes.erase(batteryRechargeEndTimes.begin()+l);
            }
        }
        
        vector<int> randomVehicleOrder;
        vector<bool> canAddRecharge;
        for (int l = 0; l < num_machines(); l++) {
            canAddRecharge.push_back(false);
            randomVehicleOrder.push_back(l);
            int setup = getSetup(vehiclePredecessorTask[l],num_tasks());
            if (vehicleBusyTime[l] == i && vehiclePredecessorTask[l] == num_tasks()) {
                num_battery--;
            }
        }
        std::random_shuffle(randomVehicleOrder.begin(), randomVehicleOrder.end());
        // Find a free machine        
            for (int l = 0; l < num_machines(); l++) {
                int k = randomVehicleOrder[l];
                vector<int> validTasks;
                for (int j = 0; j < num_tasks(); j++) {
                    int setup = getSetup(vehiclePredecessorTask[k],j);
                    if (!taskScheduled[j] // task is not scheduled
                            && ins.getStarttimes()[j]-setup <= i // minstart-setup should be less current time
                            && ins.getDeadlines()[j] >= (i + ins.getDurations()[j]+setup)
                            && ins.getVehicleStartTimes()[k] < i) { // deadline should be met
                        validTasks.push_back(j);
                    }
                }
                std::random_shuffle(validTasks.begin(), validTasks.end());
                
                if (validTasks.size() == 0 && batteryRechargeEndTimes.size() >= ins.getBatteryResourceAmount()) {
                    continue;
                } else if (validTasks.size() == 0) {
                    validTasks.push_back(-2);
                }
                
                for (int m = 0; m < validTasks.size(); m++) { 
                    int j = validTasks[m];
                    
                    // if the machine is free at the time
                    int setup;
                    if (vehicleBusyTime[k] <= (i)) {
                        int taskIndex = vehicleTaskNum[k];

                        // If enough battery available, schedule task
                        if (j != -2 && (vehicleBattery[k]-ins.getMinMaxBattery()[0]) >= ins.getBatterycost()[j]) {
                            setup = getSetup(vehiclePredecessorTask[k],j);
                            // update the solution
                            sol.vehicles[k].tasks[taskIndex].startTime = i+setup;
                            sol.vehicles[k].tasks[taskIndex].processingTime = ins.getDurations()[j];
                            sol.vehicles[k].tasks[taskIndex].label = j;
                            sol.vehicles[k].tasks[taskIndex].endTime = 
                                    sol.vehicles[k].tasks[taskIndex].processingTime
                                    + sol.vehicles[k].tasks[taskIndex].startTime;
                            sol.vehicles[k].tasks[taskIndex].battery = vehicleBattery[k]-ins.getBatterycost()[j];
                            sol.vehicles[k].tasks[taskIndex].setupTime = setup;
                            sol.vehicles[k].tasks[taskIndex].setupToDepot = getSetup(j,num_tasks());
                            
                            // update invariants
                            vehicleBattery[k] -= ins.getBatterycost()[j];
                            vehicleBusyTime[k] = sol.vehicles[k].tasks[taskIndex].endTime;
                            vehiclePredecessorTask[k] = j;
                            vehiclePredecessorTaskBattery[k] = sol.vehicles[k].tasks[taskIndex].battery;
                            taskScheduled[j] = true;
                            numScheduledTasks++;
                            vehicleTaskNum[k]++;
                            vehicleHasFullBattery[k] = 0;
                        } else { // else schedule a battery recharge
                            // update the solution
                            if ((batteryRechargeEndTimes.size() < ins.getBatteryResourceAmount() || canAddRecharge[k] == true) && vehicleHasFullBattery[k] == 0) {
                                int pred = vehiclePredecessorTask[k];
                                int succ = num_tasks();
                                setup = getSetup(pred,succ);
                                sol.vehicles[k].tasks[taskIndex].setupTime = setup;
                                if ((i - setup) < vehicleBusyTime[k]) {
                                    sol.vehicles[k].tasks[taskIndex].startTime = i + setup;
                                } else {
                                    sol.vehicles[k].tasks[taskIndex].startTime = i;
                                }
                                int rechargeTime = (ins.getMinMaxBattery()[1]-vehicleBattery[k])/ins.batteryRechargePerTimeUnit;
                                sol.vehicles[k].tasks[taskIndex].processingTime = rechargeTime;
                                sol.vehicles[k].tasks[taskIndex].label = -1;
                                sol.vehicles[k].tasks[taskIndex].endTime = 
                                        sol.vehicles[k].tasks[taskIndex].startTime
                                        + sol.vehicles[k].tasks[taskIndex].processingTime;
                                sol.vehicles[k].tasks[taskIndex].battery = ins.getMinMaxBattery()[1];
                                sol.vehicles[k].tasks[taskIndex].setupToDepot = 0;

                                // update the invariants
                                vehicleBattery[k] = ins.getMinMaxBattery()[1];
                                vehicleBusyTime[k] = sol.vehicles[k].tasks[taskIndex].endTime;
                                vehiclePredecessorTask[k] = num_tasks();
                                vehiclePredecessorTaskBattery[k] = sol.vehicles[k].tasks[taskIndex].battery;
                                vehicleTaskNum[k]++;
                                num_battery++;
                                earliestBatteryEndTime = sol.vehicles[k].tasks[taskIndex].endTime;
                                batteryRechargeEndTimes.push_back(sol.vehicles[k].tasks[taskIndex].endTime);
                                vehicleHasFullBattery[k] = 1;
                            }
                        }
                    }
                }           
            }
    }
    
    // update makespan:
    sol.makespan = 0;
    for (int i = 0; i < num_machines(); i++) {
        for (int j = 0; j < num_tasks(); j++) {
            if (sol.makespan < sol.vehicles[i].tasks[j].endTime) {
                sol.makespan = sol.vehicles[i].tasks[j].endTime;
            }
        }
    }
}

void print_stats(ostream& os) {
    // unused
}

Solution Model::solveInstance(Instance insToRead) {
    ins = insToRead;
    Model model = Model();
    clock_t begin = clock();
    cout << "beginning run: " << endl;
    
    // ** SOLVE HERE
    model.solve();
    // ** DONE SOLVING
    cout << "Solved..." << endl;

    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    if (elapsed_secs < 0.0001) { // some min, to make it a readable decimal
        elapsed_secs = 0.0001;
    }
    sol.elapsed_time = elapsed_secs;
    return sol;
}


// number of tasks from the problem instance
int Model::num_tasks(void) {
    return ins.getNumTasks();
}
// number of machines from the problem instance
int Model::num_machines(void) {
    return ins.getNumVehicles();
}

// total number needed: num_tasks() + 3 x num_machines()
// num_tasks = regular tasks
// first num_machines = battery recharging tasks for each machine
// second num_machines = start tasks for each machine
// third num_machines = end tasks for each machine
int Model::total_tasks(void) {
    return num_tasks() + 3*num_machines();
}

int Model::max_time(void) {
    return ins.getMax();
}

int Model::getSetup(int pred, int succ) {
    int n = num_tasks();
    // -1 = no predecessor
    // n = predecessor is battery
    // else normal task
    int setup;
    if (pred == -1) { // no predecessor
        setup = ins.getSetuptimes()[succ+1];
    } else if (pred == n) { // battery task
        setup = ins.getSetuptimes()[(n+1)*(n+2)+succ+1];
    } else { // normal task
        setup = ins.getSetuptimes()[(n+2)*(pred+1)+succ+1];
    }
    return setup;
}

/* 
 * File:   instance.h
 * Author: thor
 *
 * Created on October 8, 2015, 1:43 PM
 */

#ifndef INSTANCE_H
#define	INSTANCE_H

#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <vector>       // std::vector
#include <stdlib.h>     // std::atoi

using namespace std;

class Instance { // num_tasks = n, num_vehicles = m
    int num_tasks, num_vehicles, total_tasks, max, chargeTime;
    int* durations;         // duration for all tasks [0,...,n-1]
    int* setuptimes;        // setuptimes for all combination of tasks
                            // (n+3m) x (n+3m) matrix
    int* starttimes;        // starttimes for all tasks [0,...,n-1]
    int* batterycosts;      // battery cost for each task [0,...,n-1]
    int* initialbattery;    // initial battery for each vehicle [0,...,m-1]
    int* minmaxbattery;     // [lowerbound, upperbound, battery recharge rate]
    int* deadlines;         // deadline for each task [0,...,n-1]
    int* initialSetup;      // initial setuptimes for a vehicle, simulates
                            // that the first task has to travel from the depot.
    int* vehicleStartTimes; // start times for a machine, since it could be busy
    int timelimit;
    int battery_resources;
    int goToDepotAtEnd;
    const char* secondOut;
    const char* jsBestOut;
public:
    Instance();             // use dummy instance, to test the model
    Instance(const char* input, bool dummTasks, const char* secondOut, 
        int timelimit, int battery_resources, int goToDepotAtEnd);  
    // use input given
    void printProblem();    // print out the problem instance
    
    // getters for the problem instance
    const int getNumTasks();
    const int getNumVehicles();
    const int getTotalTasks();
    const int getMax();
    const int getChargetime();
    const int getBatteryResourceAmount();
    int* getDurations();
    int* getSetuptimes();
    int* getStarttimes();
    int* getBatterycost();
    int* getInitialBattery();
    int* getMinMaxBattery();
    int* getInitialSetup();
    int* getDeadlines();
    int getGoToDepotAtEnd() { return goToDepotAtEnd; }
    int getTimelimit();
    const char* getSecondOut();
    void setSecondOut(char*);
    int* getVehicleStartTimes();
    int batSpan;
    int batteryRechargePerTimeUnit;
private:

};

#endif	/* INSTANCE_H */


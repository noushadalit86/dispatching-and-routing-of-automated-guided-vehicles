/* 
 * File:   solver.cpp
 * Author: thor
 *
 * Created on October 14, 2015, 12:48 PM
 */
#include "solver.h" // definitions and dependencies for this class

#include <cstdlib>

using namespace std;

// Example input:
// "${OUTPUT_PATH}" instances/randinst_10_5test.txt results/best.txt results/tikz.txt 0 1 1 1337

/***** Ex Input : ****
 * "${OUTPUT_PATH}"             # Output path
 * instances/randinst_10_5.txt  # input file
 * results/best.txt             # output stats of best solution
 * results/tikz.txt             # output best solution to tikz gantt chart
 * 0                            # Timelimit in sec 0 = no limit
 * 5                            # battery resources available (3 default)
 * 1                            # go to depot at end
 * 1337                         # random seed
 */
int main(int argc, char** argv) {
    // if help requested
    /*if (argc == 1) {
        if (strcmp(argv[0],"help") == 0) {
            cout << "help: "
                    "please specify: 'inputfile.txt' "
                    "'bestout.txt 'bestoutjs.js' 'timeout in seconds'" << endl;
        }
        return 1;
    }*/
    // check if enough arguments
    if (argc < 7) {
        cout << "too few arguments, "
                "please specify: 'inputfile.txt' "
                "output.txt 'outtikz.txt' 'timeout in seconds'"
                "'battery resources available' 'go to detop at end' 'random seed'" << endl;
        return 1;
    }
    
    char* input = argv[1];
    char* outb = argv[2];
    char* outtikz = argv[3];
    int timelimit = atoi(argv[4]);
    int battery_resources = atoi(argv[5]);
    int goToDepotAtEnd = atoi(argv[6]);
    int randomSeed = atoi(argv[7]); 
    
    // check that input file exists
    ifstream f(argv[1]);
    if (!f.good()) {
        cout << "Cannot find input file, "
                "please specify a valid input file" << endl;
        return 1;
    }
    
    // list arguments given:
    cout << "input: " << input << endl;
    cout << "output best: " << outb << endl;
    cout << "output tikz: " << outtikz << endl;
    cout << "timelimit: " << timelimit << endl;
    cout << "battery resources: " << battery_resources << endl;
    cout << "go to depot at end: " << goToDepotAtEnd << endl;
    cout << "random seed: " << randomSeed << endl;
    
    srand(randomSeed);
    // read the instance
    Instance ins = preprocess(input, outb, timelimit, battery_resources, goToDepotAtEnd);
    
    // create the solution
    Solution sol;
    // solve it!
    sol = solve(ins);
    
    sol.printSolution();
    
    // output the best solutions:
    ofstream o(outb,std::ofstream::binary);
    sol.outputSolution(o);
    // output tikz
    ofstream o_tikz(outtikz,std::ofstream::binary);
    sol.outputTikz(o_tikz);
    
    // print solution
    sol.outputSolution(cout);
    return 0;
}

Instance preprocess(const char* inputFile, const char* secondOut, 
        int timelimit, int battery_resources, int goToDepotAtEnd) {
    Instance ins(inputFile, false, secondOut, timelimit, 
            battery_resources, goToDepotAtEnd);
    ins.printProblem();
    return ins;
}

Solution solve(Instance ins) {
    Solution sol = Model::solveInstance(ins);   
    return sol;
}



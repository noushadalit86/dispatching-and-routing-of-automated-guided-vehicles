

#include "model.h"
#include "instance.h"
#include "solution.h"

#include <gecode/driver.hh>
#include <gecode/minimodel.hh>
#include <gecode/int.hh>
#include <gecode/gist.hh>
#include <gecode/search.hh>

#include <fstream>
#include <iostream>
#include <string>

using namespace Gecode;
using namespace std;

static Instance ins;
static Solution sol;

Model::Model(const SizeOptions& opt) 
    : MinimizeScript(opt),
        start(*this, total(), 0, max_time()),
        vehicles(*this, total(), 0, num_machines()-1),
        successor(*this, total(), 0, total()-1),
        battery(*this, total(), ins.getMinBattery(), ins.getMaxBattery()),
        process(*this, total(), 0, max_time()),
        makespan(*this, 0, max_time()) {
    int m = num_machines();
    int n = num_tasks();
    int r = num_battery();
    // start times should be higher than specified
    for (int i = 0; i < n; i++) {
        rel(*this, start[i] >= ins.getStarttimes()[i]);
    }
    // task must be finished before deadline
    for (int i = 0; i < n; i++) {
        rel(*this, start[i] + process[i] <= ins.getDeadlines()[i]);
    }
    for (int i = n+r; i < n+r+m; i++) {
        // start tasks battery have initial capacity.
        rel(*this, battery[i] == ins.getInitialBattery()[i-n-r]);
        // each vehicle can start at it earliest start time
        rel(*this, start[i] >= ins.getVehicleStartTimes()[i-n-r]);
    }
    
    // create predecessor array
    IntVarArray pred(*this, n+r+2*m, 0, n+r+2*m);
    channel(*this, pred, successor);
    // succ of a task is scheduled on the same vehicle
    for(int i = 0; i < n+r+m; i++) {
        element(*this, vehicles, successor[i], vehicles[i]);
    }
    // Each vehicle has a start and end task
    for (int i = n+r; i < n+r+m; i++) {
        // each vehicle has a start and end task
        rel(*this, vehicles[i] == i-n-r);
        rel(*this, vehicles[i+m] == i-n-r);
        // max end task a vehicle to be successor of next
        // to form a circuit
        if (i == n+r) { 
            rel(*this, pred[i] == i+2*m-1);
        } else {
            rel(*this, pred[i] == i+m-1);
        }
    }
    
    // Build the cost matrix
    IntVarArgs costs(*this, n+r+2*m, 0, max_time());
    IntArgs c((n+r+2*m)*(n+r+2*m));
    for (int i = 0; i < n+r+2*m; i++) {
        for (int j = 0; j < n+r+2*m; j++) {
            if (i >= n+r && j >= n+r) { // depot -> depot
                c[i*(n+r+2*m)+j] = 0;
            } else if (i >= n+r) { // depot -> task
                c[i*(n+r+2*m)+j] = ins.getSetuptimes()[n][j];
            } else if (j >= n+r) { // task -> depot
                if (ins.goToSetupAtEnd == 0) {
                    c[i*(n+r+2*m)+j] = 0;
                } else {
                    c[i*(n+r+2*m)+j] = ins.getSetuptimes()[i][n];
                }
            } else { // task -> task
                c[i*(n+r+2*m)+j] = ins.getSetuptimes()[i][j];
            }
        }
        cout << endl;
    }
    // make sure that the succ array forms a cycle
    // and put setup into 'costs'
    IntVar total_cost(*this, 0, Int::Limits::max);
    circuit(*this, c, successor, costs, total_cost, opt.icl());
    // Fill in battery cost 
    int batterySpan = ins.getMaxBattery()-ins.getMinBattery();
    IntVarArray batteryCost(*this, n+r+2*m, 0, batterySpan);
    for (int i = 0; i < n+r+2*m; i++) {
        if (i < n) {// normal task
            rel(*this, batteryCost[i] == ins.getBatterycost()[i]);
        } else if (i < n+r) {// battery recharge task
            rel(*this, batteryCost[i] == ins.batteryRechargePerTimeUnit
                    * process[i]);
        } else { // aux task
            rel(*this, batteryCost[i] == 0);
        }
    }
    // duration should only be variable for recharging tasks
    for (int i = 0; i < n+r+2*m; i++) {
        if (i < n) { // normal task
            rel(*this, process[i] == ins.getProcessTimes()[i]);
        } else if (i < n+r) { // battery tasks
            rel(*this, process[i] == batteryCost[i]/ins.batteryRechargePerTimeUnit);
        }  else { // auxiliary tasks
            rel(*this, process[i] == 0);
        }
    }
    // Set up values of predecessor to a task
    IntVarArgs sbefore(*this, n+r+2*m, 0, max_time());
    IntVarArgs pbefore(*this, n+r+2*m, 0, max_time());
    IntVarArgs stbefore(*this, n+r+2*m, 0, max_time());
    IntVarArgs bbefore(*this, n+r+2*m, ins.getMinBattery(), ins.getMaxBattery());
    for (int i = 0; i < n+r+2*m; i++) {
        // anything but start task
        if (i < n+r || i >= n+r+m) { 
            element(*this,start,pred[i],sbefore[i]);
            element(*this,process,pred[i],pbefore[i]);
            element(*this,costs,pred[i],stbefore[i]);
            // relations of start times
            rel(*this, sbefore[i] + pbefore[i] + stbefore[i] <= start[i]);
            element(*this,battery,pred[i],bbefore[i]);
            if (i >= n && i < n+r) { // Battery recharge
                // if fixed recharge
                if (ins.batRechargeType == 1) {
                    rel(*this, batteryCost[i] == ins.getMaxBattery()-bbefore[i]);
                }
                // add battery cost if recharge task
                rel(*this, bbefore[i] + batteryCost[i] == battery[i]);
            } else { // aux and normal tasks
                // subtract for all others
                rel(*this, bbefore[i]-batteryCost[i] == battery[i]);
            }
        }
    }
    // if vehicles has same min start and initial battery
    // Set up limit to how many battery recharges we can have concurrently
    if ( r > ins.getBatteryResourceAmount() && m > ins.getBatteryResourceAmount()) {
        IntVarArgs end(*this, r, 0, max_time());
        for (int i = n; i < r+n; i++) {
            rel(*this, start[i] + process[i] == end[i-n]);
        }
        IntArgs limit = IntArgs::create(r,1,0);
        cumulative(*this, ins.getBatteryResourceAmount(), start.slice(n,1,r), 
                process.slice(n,1,r), end, limit);
    }
    // bounds for makespan
    rel(*this, makespan >= ins.getLowerBound());
    rel(*this, makespan <= ins.getUpperBound());
    // makespan is max of start times (of end auxiliary tasks)
    max(*this, start, makespan);
    // Vehicle symmetry
    for (int i = 0; i < m; i++) {
        for (int j = i; j < m; j++) {
            if (i != j 
                    && ins.getVehicleStartTimes()[i] 
                    == ins.getVehicleStartTimes()[j]
                    && ins.getInitialBattery()[i] 
                    == ins.getInitialBattery()[j]) {
                rel(*this, successor[i+r+n] <= successor[j+r+n]); 
            }
        }
    }
    // battery symmetry
    Symmetries syms;
    syms << VariableSymmetry(successor.slice(n,1,r));
    // branchers
    branch(*this, successor, INT_VAR_MAX_MAX(), INT_VAL_MIN(), syms);
    branch(*this, vehicles, INT_VAR_NONE(), INT_VAL_RND(Rnd(1337)));
    branch(*this, start, INT_VAR_NONE(), INT_VAL_MIN());
    branch(*this, process, INT_VAR_NONE(), INT_VAL_MIN());
    branch(*this, battery, INT_VAR_NONE(), INT_VAL_MIN());
    branch(*this, makespan, INT_VAL_MIN());
}

  // Return cost
    IntVar Model::cost(void) const {
        return makespan;
    }

  // Print solution
void Model::print(ostream& os) const {
    if (makespan.assigned()) {
        sol.makespan = makespan.val();
    }
    int m = num_machines();
    int n = num_tasks();
    int r = num_battery();
    sol.num_machines = m;
    sol.num_tasks = n;
    sol.num_battery = r;
    sol.tasks = new int[n+r];
    sol.setuptimes = new int[n+r];
    sol.processingtimes = new int[n+r];
    sol.tasksPerVehicle = new int[m];
    sol.starttimes = new int[n+r];
    sol.vehicles = new Vehicle[m];
    for (int i = 0; i < m; i++) {
        sol.vehicles[i].tasks = new Task[n+r];
        for (int j = 0; j < n+r; j++) {
            // if processing time is -1 = task is not scheduled
            sol.vehicles[i].tasks[j].processingTime = -1;
            sol.vehicles[i].tasks[j].label = -1;
            sol.vehicles[i].tasks[j].setupTime = -1;
            sol.vehicles[i].tasks[j].startTime = -1;
        }
    }
    // set that we do not have any tasks on a vehicle
    for (int i = 0; i < m; i++) {
        sol.tasksPerVehicle[i] = 0;
    }
    cout << "-----------------------------------" << endl;
    cout << "total cost is: " << makespan << endl;
    cout << "scheduled: " << vehicles << endl;
    cout << "succ: " << successor << endl;
    cout << "start: " << start << endl;
    cout << "duration: " << process << endl;
    cout << "battery: " << battery << endl;
    if (successor.assigned() && vehicles.assigned()) {
        int p = n+r+2*m-1; // start with last auxiliary task
        int taskNum = 0;
        int totalTasks = 0;
        int v = 0;
        int taskNum_v = 0;
        int setup = 0;
        cout << "first task is: " << p << endl;
        while (totalTasks < n+r+2*m) {
            int c = successor[p].val();
            v = vehicles[c].val();
            if (c < n+r) {
                if (vehicles[c].val() == vehicles[p].val()) {// if same vehicle
                    // setup is present
                    setup = start[c].val()-start[p].val()-process[p].val();
                } else { // we have no setup
                    setup = 0;
                    taskNum_v = 0;
                }
                int label = c;
                if (c >= n) { // if battery -> correct label
                    label = -1;
                } else {
                }
                sol.tasks[taskNum] = label;
                sol.setuptimes[taskNum] = setup;
                sol.starttimes[taskNum] = start[c].val();
                sol.processingtimes[taskNum] = process[c].val();
                // second view
                sol.vehicles[v].tasks[taskNum_v].label = label;
                sol.vehicles[v].tasks[taskNum_v].setupTime = setup;
                sol.vehicles[v].tasks[taskNum_v].startTime = start[c].val();
                sol.vehicles[v].tasks[taskNum_v].processingTime = process[c].val();
                sol.tasksPerVehicle[v]++;
                taskNum_v++; 
                taskNum++;
            }
            totalTasks++;
            p = c;
        }
        cout << "succ visualization: " << endl;
        for (int i = 0; i < m; i++) { // for each vehicle
            int dur = 0;
            cout << "vehicle " << i << ":" << endl;
            for (int j = 0; j < n+r+2*m; j++) { // for each task
                int label = j;
                if (vehicles[j].val() == i) {
                    if (j >= n+r) { // aux
                        if (j < n+r+m) {
                            cout << "STA ";    
                        } else {
                            cout << "END ";
                        }
                        dur = start[j].val();
                    } else if (j >= n) { // battery
                        cout << "BAT ";
                        label = -1;
                    } else{ // normal task
                        cout << "    ";
                    }
                    cout << j << "\t-> " << successor[j].val() 
                            << "\t start: " << start[j].val()
                            << "\t dur: " << process[j].val()
                            << "\t bat: " << battery[j].val() << endl;
                }
            }
            cout << "Total Duration: " << dur << endl;
        }
    }
    cout << "best solution so far:" << endl;
    sol.outputSolution(cout);
    ofstream o(sol.outb,std::ofstream::binary);
    sol.outputSolution(o);
}

void print_stats(Search::Statistics &stat, ostream& os) {
        os << "\ttime: " << sol.elapsed_time << endl;
        os << "\tsolution: " << sol.makespan << endl;
        os << endl;
        os << "\tfail: " << stat.fail << endl;
	os << "\tnodes: " << stat.node << endl;
	os << "\tpropagators: " << stat.propagate << endl;
	os << "\tdepth: " << stat.depth << endl;
	os << "\trestarts: " << stat.restart << endl;
	os << "\tnogoods: " << stat.nogood << endl;
}

  // Constructor for cloning s
Model::Model(bool share, Model& s) : MinimizeScript(share,s) {
    start.update(*this, share, s.start);
    process.update(*this, share, s.process);
    vehicles.update(*this, share, s.vehicles);
    battery.update(*this, share, s.battery);
    successor.update(*this, share, s.successor);
    makespan.update(*this, share, s.makespan);
}

  // Copy during cloning
Space* Model::copy(bool share) {
    return new Model(share,*this);
}

Solution Model::solveInstance(Instance insToRead, 
        const char* bout) {              
    // initiate the first solution outputs;
    sol.didFirstOut = false;
    sol.setupsToDepot = insToRead.getSetupsToDepot();
    sol.outb = strdup(bout);

    ins = insToRead;
    SizeOptions opt("Cumulatives_setuptimes");
    opt.solutions(0);
    opt.time(ins.getTimelimit()*1000);
    opt.threads(2);
    if (ins.getTimelimit() == 0) {
        cout << "timelimit is 0 -> unlimited" << endl;
        opt.time(99999*1000);
    }
    clock_t begin = clock();
    cout << "beginning run: " << endl;
    bool first = true;
    Search::Options so;
    Search::TimeStop* ts;
    ts = new Search::TimeStop(opt.time());
    so.stop = ts;
    Search::Statistics stat;
    // Timer
    bool foundSolution = false;
    bool initial = true;
    while (!foundSolution) {
        if (!initial) {
            cout << "------------------------------------------------" << endl;
            cout << "No solution found with " <<ins.getNumRecharges() <<
                    " recharges , adding one" << endl;
            cout << "------------------------------------------------" << endl;
            ins.addBatteryRecharge();
        }
        Support::Timer t;
        t.start();
        
        Model* m = new Model(opt);
        
        // space status
        SpaceStatus status = m->status(stat);
        print_stats(stat, cout);
        if (status == SS_FAILED)
                cout << "Status: " << m->status() << " the space is failed at root."
                                << endl;
        else if (status == SS_SOLVED)
                cout << "Status: " << m->status()
                                << " the space is not failed but the space has no brancher left."
                                << endl;
        else if (status == SS_BRANCH)
                cout << "Status: " << m->status()
                                << " the space is not failed and we need to start branching."
                                << endl;
        m->print(cout);
        
        // run model
        BAB<Model> e(m, so);
        delete m;
        while (Model* s = e.next()) {
            s->print(cout);
            cout << "found solution" << endl;
            foundSolution = true;
            stat = e.statistics();
            print_stats(stat, cout);

            cout << "\ttime: " << t.stop() / 1000 << "s" << endl;
            delete s;
        }
        if (e.stopped()) {
            foundSolution = true;
            cout << "WARNING: solver stopped, solution is not optimal!\n";
            if (ts->stop(e.statistics(), so)) {
                    cout << "\t Solver stopped becuase of TIME LIMIT!\n";
            }

        }
        // output the stats
        cout << "outputting currently best solution to: ";
        string str = bout;
        cout << str.substr(0,str.length()-4) << endl;
        char stat_out[100];
        strcpy(stat_out, str.substr(0,str.length()-4).c_str());
        strcat(stat_out, "_stat.txt");
        ofstream o(stat_out,std::ofstream::binary);
        print_stats(stat,o);
        cout << "stat_out: " << stat_out << endl;
        print_stats(stat, cout);
        cout << "\ttime: " << t.stop() / 1000 << "s" << endl;

        clock_t end = clock();
        double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
        sol.elapsed_time = elapsed_secs;
        cout << "elapsed time: " << elapsed_secs << endl;
        initial = false;
        // if no solution is found with one battery recharge per task
        // there exist no solution
        if (ins.getNumRecharges() == ins.getNumTasks()) {
            foundSolution = true;
        }
    }
    return sol;
}

// number of tasks from the problem instance
int Model::num_tasks(void) const {
    return ins.getNumTasks();
}
// number of machines from the problem instance
int Model::num_machines(void) const {
    return ins.getNumVehicles();
}

int Model::total_tasks(void) {
    return num_tasks() + num_battery() + num_machines();
}

int Model::max_time(void) const {
    return ins.getMax();
}

int Model::num_battery(void) const {
    return ins.getNumRecharges();
}

int Model::total(void) const {
    return num_tasks() + num_battery() + 2*num_machines();
}

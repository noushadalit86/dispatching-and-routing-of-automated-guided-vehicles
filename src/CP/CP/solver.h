/* 
 * File:   solver.h
 * Author: thor
 *
 * Created on October 14, 2015, 12:54 PM
 */

#ifndef SOLVER_H
#define	SOLVER_H

#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <vector>       // std::vector
#include <stdlib.h>     // std::atoi

#include "model.h"      // gecode model
#include "instance.h"   // Instance of a problem
#include "solution.h"   // Repesentation of a solution

// Gecode
#include <gecode/driver.hh>
#include <gecode/minimodel.hh>
#include <gecode/int.hh>
#include <gecode/gist.hh>

using namespace std;

Instance preprocess(const char* inputFile, const char* outB, int timelimit, 
        int battery_resources, int setupAtEnd, int batteryRechargeType, int StartNumRecharges);
Solution solve(Instance toSolve, const char* outB);

#endif	/* SOLVER_H */


/* 
 * File:   solver.cpp
 * Author: thor
 *
 * Created on October 14, 2015, 12:48 PM
 */
#include "solver.h" // definitions and dependencies for this class

#include <cstdlib>

using namespace std;

/* Example Input:
 "${OUTPUT_PATH}"
 * instances/randinst_10_5_fixed.txt // input file
 * results/best.txt // output file
 * results/tikz.txt // output tikz
 * 0 // timeout in seconds
 * 3 // num battery recharges
 * 0 // go to depot end or not // 0 = dont go, 1 = go to depot
 * 0 // Always recharge to max or not // 0 = do not, 1 = always recharge to max
 * 3 // num recharges available
 */

int main(int argc, char** argv) {
    // if help requested
    cout << "argc: " << argc << endl;
    if (argc == 1) {
        if (strcmp(argv[0],"help") == 0) {
            cout << "help: "
                    "please specifyy: "
                    "'intputfile.txt' "
                    "'outputfile.txt' "
                    "'outputtikz.txt' "
                    "'timeout in seconds' "
                    "'Number of battery recharge stations' "
                    "'At end go to depot or not {0,1}' "
                    "'Always recharge to max {0,1}'" << endl;
        }
        return 1;
    }
    // check if enough arguments
    if (argc < 6) {
        cout << "too few arguments, "
                    "'intputfile.txt' "
                    "'outputfile.txt' "
                    "'outputtikz.txt' "
                    "'timeout in seconds' "
                    "'Number of battery recharge stations' "
                    "'At end go to depot or not {0,1}' "
                    "'Always recharge to max {0,1}'" << endl;
        return 1;
    }
    ifstream f(argv[1]);
    if (!f.good()) {
        cout << "Cannot find input file, "
            "please specify a valid input file" << endl; 
        return 1;
    }
    char* input = argv[1];
    char* outb = argv[2];
    char* outtikz = argv[3];
    int timelimit = atoi(argv[4]);
    int battery_resources = atoi(argv[5]);
    int setupAtEnd = atoi(argv[6]);
    int batRechargeType = atoi(argv[7]);
    int startNumRecharges = 0;
    if (argc > 7) {
        startNumRecharges = atoi(argv[8]);
    }
    
    cout << "input: " << input << endl;
    cout << "output best: " << outb << endl;
    cout << "output tikz: " << outtikz << endl;
    cout << "timelimit: " << timelimit << endl;
    cout << "battery resources: " << battery_resources << endl;
    cout << "goToSetupAtEnd: " << setupAtEnd << endl;
    cout << "batRechargeType: " << batRechargeType << endl;
    cout << "start num recharges: " << startNumRecharges << endl;
    // read the instance
    Instance ins = preprocess(input, outb, timelimit, 
            battery_resources, setupAtEnd, batRechargeType, startNumRecharges);
    // create the solution
    Solution sol;
    // solve it
    sol = solve(ins, outb);
    sol.setupAtEnd = setupAtEnd;
    sol.setupsToDepot = ins.getSetupsToDepot();
    
    ofstream o(outb,std::ofstream::binary);
    sol.outputSolution(o);
    ofstream o_tikz(outtikz,std::ofstream::binary);
    sol.outputTikz(o_tikz);
    return 0;
}

Instance preprocess(const char* inputFile,
        const char* outB, int timelimit, int battery_resources, int setupAtEnd, 
        int batRechargeType, int startNumRecharges) {
    Instance ins(inputFile, outB, timelimit, battery_resources, 
            setupAtEnd, batRechargeType, startNumRecharges);
    ins.printProblem();
    return ins;
}

Solution solve(Instance ins, const char* bout) {
    ins.setValIndex(-1);
    ins.setVarIndex(-1);
    Solution sol = Model::solveInstance(ins, bout);
    return sol;
}


\chapter{Mixed Integer Linear Programming}
\label{mip}
In this section we will present a mixed integer linear programming approach to solving the problem of scheduling tasks to vehicles. We will present a model which handles multiple recharges and a model which only offers one per vehicle. The models we describe are an extension of the one presented in Artigues et al. \cite{Artigues2015} (2015), combined with elements and notation of the model presented in Avalos-Rosales et al. \cite{Avalos-Rosales2013} (2013). The assignment variables used in \cite{Avalos-Rosales2013} are one of those elements. In their paper an assignment is represented by the binary decision variable $x_{ijk}$, which represents the
assignment of task $k$ immediately after task $j$ on vehicle $i$. 
%In our work we will use $x_{ijk}$ to represent the assignment of task $j$ to vehicle $k$ after task $i$ in order to be cohesive with later sections. 
In \cite{Artigues2015} the authors present models for solving RCPSP (Resource constrained project scheduling problem) with both production and consumption of resources. In our case the resource is the battery capacity for a vehicle. Production is the action of recharging the battery, while consumption is the power depletion as a result of performing tasks. They use a similar assignment scheme, however the assignment variables implicate any predecessor/successor in the sequence, not just immediate neighbours. If some task $j$ occurs at any point in the same machine schedule before $k$, then they will be marked as predecessor/successor. We chose to use the first scheme because it allows for easier scheduling of events in-between tasks (such as recharging). The flow-based approach from this article is extended to scheduling for $m$ parallel machines, much like the flow-based approach used in \cite{Avalos-Rosales2013}, while retaining the extra constraints dealing with consumption/production.
\medskip
\\
In the next section we will present the basic model for scheduling tasks on vehicles. We will then extend this model with constraints allowing for a fixed amount of recharging. In the following sections we will modify this initial simple model such that it can allow for flexible recharging. This will be for both the case of only recharging what is needed to complete a schedule and the case of always recharging to full capacity when a recharge is scheduled to happen.
\section{Basic Model with Fixed Recharge}
\label{sec:mipbasic}
We will start off with the basic model for building a schedule while minimizing makespan. Later, we will extend it with setup times and battery regulation.
Our binary assignment variables represent the assignment of task $k$ to machine $i$ immediately after task $j$. The Equation \eqref{eq:mip1} defines the domain of said variables. Next we define the main relations dealing with the actual assignment. Any task may only be assigned once to a predecessor on one machine and similarly for successors. In order to schedule the first task on each machine, we assign it as a successor to the dummy task, task zero. The last task is similarly assigned to dummy task $n+1$.\\
\smallskip
\\
Their function is similar to a source and sink for the schedule (see fig \ref{fig:schedulefig}).
\begin{figure}

\includegraphics{schedule.eps}
\caption{An illustration of the starting and finishing tasks as source and sink for schedule.}
\label{fig:schedulefig}
\end{figure}
\\Based on this, we must make sure that start and end variables do not have a predecessor and successor respectively.
\begin{flalign} x_{ijk} \in \{0,1\} \qquad &\forall i \in M ,\ \forall j,k \in N_s, \ j \neq k \label{eq:mip1}\\
\sum_{\substack{k \in N_{0} 
\\ j \neq k}} \sum_{i \in M} x_{ikj} = 1\qquad &\forall j \in N \label{eq:mip2}\\
\sum_{\substack{k \in N_{1} 
\\ j \neq k}} \sum_{i \in M} x_{ijk} = 1\qquad &\forall j \in N \label{eq:mip3}
\end{flalign}
The domain of the assignment variable is defined in eq (\ref{eq:mip1}) to be over binary values. Constraint (\ref{eq:mip2}) makes sure that at most one task is assigned as a successor to task zero on any given vehicle. Equation (\ref{eq:mip3}) ensures only one predecessor for the last task on each vehicle. 
%Constraint (\ref{eq:mip4}) ensures that no task comes before task zero on any machine, while (\ref{eq:mip5}) ensures no task comes after $n+1$ for any machine.
\\
Next, we must ensure that only one task follows the source task per vehicle. This can be a regular task $j \in N$ or the sink task $n+1$ on any given vehicle. 
\begin{align} \sum_{k\in N_{1}} x_{i0k} = 1\qquad &\forall i \in M \label{eq:mip6}
\\
\sum_{j\in N_0} x_{ijn+1} = 1\qquad &\forall i \in M \label{eq:mip7}
\end{align}
If no real tasks are scheduled on some vehicle $i$, the predecessor of task $n+1$ on vehicle $i$ will be task zero on the same vehicle. Hence the strict equality in both relations.
\medskip
\par
Next, we must connect the variables representing the tasks such that the assigned variables form a continuous schedule for each vehicle in a given solution. If a predecessor has been assigned for task $j$ on machine $i$, we must make sure a corresponding successor is assigned on that machine as well. The next constraint makes sure that this is the case. 
\begin{equation} \sum_{\substack{k \in N_{1} 
\\ j \neq k}}  x_{ijk}  - \sum_{\substack{h \in N_0 \\ j \neq h}}  x_{ihj} = 0\qquad \forall j \in N,\ \forall i \in M \label{eq:mip8}
\end{equation}
Next, we must define the equations dealing with completion time and the minimization of objective function. In this case we want to minimize makespan. For this we define the continuous variable $C_j$ to represent the completion time of task $j$. Because we are dealing with time, the domain of $C_j$ will be non-negative. 
%In order to assign $C_k$ the finishing time in relation to the task sequence we restrict the completion time of a successor task $k$: 
\begin{align} C_{j} \geq 0\qquad &\forall j \in N \label{eq:mip9}
\\C_k - C_{j} + P(1 - x_{ijk}) \geq p_k\qquad \begin{split}&\forall i \in M ,\ \forall j \in N_0,\\ &\forall k \in N_{1}, \ j \neq k\end{split} \label{eq:mip10}
\end{align}
If a task $k$ follows $j$ on a machine $i$, then Equation \eqref{eq:mip10} imposes that the completion time of task $k$ must be at least greater than the completion time of $j$ plus the amount of time spent on processing task $k$. If they do not follow each other (i.e. $j$ and $k$ are not predecessor/successor) or they are scheduled on different vehicles, then there will be nothing to impose as the large integer $P$ will be added to the left-hand side of the relation.
\medskip
\par
Next, we will introduce setup times to the model. Setup times deals with the time it takes to get ready between two tasks. This time is based on the distance between the destination of the previous task and the origin of the next beginning task. We modify Equation \eqref{eq:mip10} to include this new parameter.
\begin{align}
C_k - C_{j} + P(1 - x_{ijk}) \geq p_k + s_{jk}\qquad \begin{split}&\forall i \in M ,\ \forall j \in N_0,\\ &\forall k \in N_{1}, \ j \neq k\end{split} \label{eq:mip10.1}
\end{align}
Certain tasks may need to be completed within a set time window. These time windows are given by an arrival time which signifies when the task will be available to process and deadlines restricting the latest completion time of the task. The next constraints make sure any task with a deadline is completed before said time and that any task with an arrival time begins after the stated time.
\begin{align} C_j \leq d_j \qquad &\forall j \in N \label{eq:mip11}
\\ C_j - p_j \geq a_j \qquad &\forall j \in N \label{eq:mip12}
\end{align}
Finally we include the objective function minimizing the makespan.
\begin{equation} C_j \leq C_{\max}\qquad \forall j \in N \label{eq:mip13}
\end{equation}
\begin{equation}\text{minimize } C_{\max} \label{eq:mip14}
\end{equation}
This concludes the description of the main components.

\section{Battery Recharge Constraints}
\label{sec:mipbattery}
In order to be able to represent and update the battery resource, it is necessary to apply a couple of extensions to the basic model described in the previous section. In this section we will present these extra constraints for three variations of handling recharging. First, we will present battery resource consumption from regular tasks being scheduled as well as simple recharging with a fixed constant amount.
Afterwards, we will introduce modifications such that we can recharge flexibly to full capacity based on the given battery level. Finally, we will extend this further and allow for pure flexibility in the form of being able to charge just what is needed for the schedule, as opposed to always full capacity. The flexible modifications are implemented using recharge modes based on the piecewise function described in Section \ref{sec:rechargemodes}.
\subsection{Fixed Recharging}
\label{sub:mipfixed}
In this subsection we fill present the basic extensions for handling both consumption and production for battery resources. We will present constraints for recharging a fixed amount.
All tasks consume battery power as they are executed. In order to schedule the act of recharging a vehicle, a pseudo-task represented by the binary variable $z_{ijk}$ is included in the model. The variable represents the task of recharging the vehicle being scheduled after $j$ and prior to the task $k$ on machine $i$.
\begin{align} z_{ijk} \in \{0,1\} \qquad &\forall j \in N_0, \ \forall k \in\ N_{1},\ \forall i \in M \label{eq:mip15}
\\ x_{ijk}\geq z_{ijk} \qquad &\forall j \in N_0, \ \forall k \in\ N_{1},\ \forall i \in M \label{eq:mip16}
\end{align}
The assignment variable for a battery charge event may only be set if the corresponding variable for task scheduling is set as well. Constraint (\ref{eq:mip16}) covers this.
\medskip
\par
In order to represent the consumption and production of resources the model requires the addition of more parameters. Let the parameter $c_j^-$ be the battery power consumption necessary in order to perform task $j$. Let $f^f$ be the time spent on recharging any given vehicle. Let the parameter $c^+$ be the general amount of battery power which is generated from charging any given vehicle. The available amount of battery power by task $j$ on vehicle $i$ is defined as the positive linear variable $o_{ij}$. The consumption of resources is based on the setup time and processing time at task $j$, while the production is based on the scheduling of battery charging tasks.\\
\begin{equation} o_{ij} \geq 0 \qquad \forall j\ \in N_{s} \label{eq:mip17}
\end{equation}
In the next couple of equations, we will deal with the production and consumption of resources using these new parameters and the $o_{ij}$ variable. 
In simple terms, a successor $k$ must have a production corresponding to that of its predecessor $j$ minus the consumption $c_k^-$ of task $k$ and plus some fixed charge $c^+$ if charging has been scheduled. 
This requires us to build up our relation based on three cases: a recharge is scheduled, no recharge is scheduled but they are predecessor/successor and finally the tasks are not neighbours. The first case describes the situation where $j$ is the predecessor of $k$ (i.e. they have also been assigned to the same machine) and a recharge event is set to occur between them. In the second case $j$ is followed by $k$, but no charging occurs between them. In the third case $j$ and $k$ have no relation (one does not follow the other and/or they do not occur on the same machine) in which case we want $o_{ij}$ to be independent of $o_{ik}$ and vice versa.
\begin{align} o_{ik}  \leq o_{ij} +c^+z_{ijk}- c_i^- + P(1 - x_{ijk}) \qquad \begin{split}&\forall i \in M, \ \forall j \in N_0,\\
&\forall k \in N_1, j \neq k\end{split}  \label{eq:mip20}
\end{align}
Constraints \eqref{eq:mip20} cover all three cases. In case one, $o_{ik}$ becomes equal to the level at $o_{ij}$ minus the consumption of task $k$ plus the recharged amount $c^+$. In case two, $o_{ik}$ becomes equal to $o_{ij}$ minus the consumption $c_k^-$. The recharge term becomes zero and no recharge amount is added. In the last case the tasks involved have no direct relation, so the relation becomes $o_{ik} \leq P$.
Next, we want to ensure that ensure that $o_{ik}$ behaves like a battery, in that we can only charge to full capacity $f$. We also want to make sure that we never exceed a minimum level of battery charge $q$ while we are executing a schedule.
\begin{align} o_{ik} \leq f \qquad &\forall i \in M, \ \forall k \in N  \label{eq:mip21}\\
o_{ik} \geq q \qquad &\forall i \in M, \ \forall k \in N  \label{eq:mip22}
\end{align}
Constraints \eqref{eq:mip21} limit the $o_{ik}$ to the given capacity, while \eqref{eq:mip22} set the minimum level of battery charge at a given task.\\
Each vehicle starts with some level of battery charge $l_i$. In order to get the starting level into our model, we add the following equation for each source task of each vehicle.
\begin{equation} o_{i0}  = l_i \qquad \forall i \in M  \label{eq:mip23}
\end{equation}
\medskip
\par
Finally in order to link the charging event to the completion time we add the time spent charging to Constraints \eqref{eq:mip10.1}. We then multiply this value with some constant $\epsilon$, representing the amount of time spent on charging a single unit of battery charge. This will ensure that the scheduling of recharging events is implicitly minimized, such that we only schedule charging when necessary. Later when flexible recharging is introduced, this will also allow us to minimize the amount recharged to only the amount needed to complete the schedule.\\
For this we split the relation into two in order to handle all the cases described earlier for setting $o_{ik}$. The setup time will vary based on whether or not recharging has been scheduled. If a recharge event is scheduled, then the setup time will be the time it takes to go to the depot from the previous task and the time it takes to go to the next task from the depot.

\begin{align} C_k - C_{j} + P(1 - x_{ijk}) + P(1 - z_{ijk})\geq p_k + s_{j0} + s_{0k} + (c^+)\epsilon \qquad \begin{split}&\forall i \in M ,\ \forall j \in N_0,\\ &\forall k \in N_1,\ j \neq k \end{split} \label{eq:mip27}
\\ C_k - C_{j} + P(1 - x_{ijk}) \geq p_k + s_{jk}\qquad \begin{split}&\forall i \in M ,\ \forall j \in N_0,\\ &\forall k \in N_{1},\ j \neq k\end{split} \label{eq:mip28} 
\end{align}
Relation \eqref{eq:mip27} sets the completion time of task $k$ in the case of recharging being scheduled, while \eqref{eq:mip28} sets completion time of $k$ when only a regular assignment is made.
\bigskip
\par
Finally, we show the complete model with fixed amount recharge in Fig. \ref{fig:mipfixed}.
\begin{figure}
\caption{Basic model with fixed recharging added.}
\label{fig:mipfixed}
\begin{equation*}\text{minimize } C_{\max} 
\end{equation*}
\begin{align*}  
\sum_{\substack{j \in N_{0} 
\\ j \neq k}} \sum_{i \in M} x_{ijk} = 1 \qquad &\forall k \in N \\
\sum_{\substack{j \in N_{1} 
\\ j \neq k}} \sum_{i \in M} x_{ikj} = 1 \qquad & \forall k \in N \\
\sum_{k\in N_{1}} x_{i0k} = 1\qquad & \forall i \in M \\
\sum_{j\in N_0} x_{ijn+1} = 1\qquad & \forall i \in M \\
\sum_{\substack{k \in N_{1} 
\\ j \neq k}}  x_{ijk}  - \sum_{\substack{h \in N_0 \\ j \neq h}}  x_{ihj} = 0\qquad & \forall j \in N,\ \forall i \in M\\
x_{ijk}\geq z_{ijk}\qquad &  \forall j \in N_0, \ \forall k \in\ N_{1},\ \forall i \in M \\
o_{ik}  \leq o_{ij} +c^+z_{ijk}- c_i^- + P(1 - x_{ijk})\qquad \begin{split}&\forall i \in M, \ \forall j \in N_0,\\
&\forall k \in N_1, j \neq k\end{split}\\
o_{ik} \leq f \qquad &\forall i \in M, \ \forall k \in N  \\
o_{ik} \geq q  \qquad &\forall i \in M, \ \forall k \in N \\
o_{i0}  = l_i \qquad &\forall i \in M \\
\begin{aligned}C_k - C_{j} + P(1 - x_{ijk}) + P(1 - z_{ijk}) \\ \geq p_k + s_{j0} + s_{0k} + (c^+)\epsilon \end{aligned}\qquad &\begin{aligned} \forall i \in M ,\ \forall j \in N_0,\\ \forall k_1 \in N,\ j \neq k \end{aligned}\\
C_k - C_{j} + P(1 - x_{ijk}) \geq p_k + s_{jk}\qquad \begin{split}&\forall i \in M ,\ \forall j \in N_0,\\ &\forall k \in N_{1},\ j \neq k\end{split}\\
C_j \leq d_j \qquad &\forall j \in N \\
C_j - p_j \geq g_j \qquad &\forall j \in N\\
C_j \leq C_{\max} \qquad &\forall j \in N\\
o_{ij} \geq 0 \qquad &\forall i \in M, \ \forall j\ \in N_s \\
z_{ijk} \in \{0,1\} \qquad &\forall j \in N_0 \ \forall k \in\ N_{1} ,\ \forall i \in M\\
x_{ijk} \in \{0,1\} \qquad &\forall i \in M ,\ \forall j \in N_0,\ \forall k \in N_{1} \ j \neq k \\ 
C_{j} \geq 0 \qquad &\forall j \in N 
\end{align*}

\end{figure}
\subsection{Full Flexible Recharging}
\label{sub:mipfullflex}
In this section we will describe the modifications which are necessary in order to make the model presented in Subsection \ref{sub:mipfixed} able to perform flexible recharging. 
The model presented in \ref{sub:mipfixed} can only charge a fixed amount in the form of a given parameter constant, but this section will provide the variables, parameters and constraints for flexible recharging to full capacity. In order to do this, we need to be able to compute how long it takes, now that the amount we recharge is not predetermined.
We introduce the variable linear variable $c_{ij}^t$ representing the time spent recharging. For recharge modes, we use the amount of recharge generated in order to calculate the amount of time spent. For this, let $\omega \in \Omega$ be the set of modes we can recharge for a given instance. Let $H_\omega$ be the slope and let $\eta_\omega$ be the y-intercept of the linear function representing the mode $\omega$. Finally, let the binary variable $z_{i j \omega}^m$ represent the fact that some recharge amount at task $j$ on vehicle $i$ is within the thresholds of some mode $\omega$. This binary variable solely represents the mode for a given battery charge level $o_{ij}$. We will use this time in order to compute the relative time spent recharging, by subtracting the time at $o_{ij}$ from the absolute time at the amount $f$. Recall that the time spent to charge to full capacity from zero capacity was defined as $f^t$.
\medskip
\par
First, let us update the Relation \eqref{eq:mip20} describing the change in resources. We update \eqref{eq:mip20} by replacing the constant amount recharge with the new constant point to which we recharge, $f$. Since we now have the amount we recharge to given, we no longer need to include the battery level of the previous task, $o_{ij}$. We split the relation into two, one for recharging and one for no scheduled recharge task.
\begin{align} o_{ik}  \leq f- c_i^- + P(1 - x_{ijk})+P(1-z_{ijk}) \qquad \begin{split}&\forall i \in M, \ \forall j \in N_0,\\
&\forall k \in N_1, j \neq k\end{split}  \label{eq:mip20-varfull}\\
o_{ik}  \leq o_{ij} - c_i^- + P(1 - x_{ijk}) +P(z_{ijk})\qquad \begin{split}&\forall i \in M, \ \forall j \in N_0,\\
&\forall k \in N_1, j \neq k\end{split}  \label{eq:mip21-varfull}
\end{align}
Constraints \eqref{eq:mip20-varfull} handle the case of a recharge being scheduled between tasks $j$ and $k$. In the new constraints we replace the constant $c^+$ with the parameter $f$, such that the battery charge level is always set to full capacity when a recharge is performed. Constraints \eqref{eq:mip21-varfull} deal with just normal tasks being scheduled.\medskip
\par
\begin{figure}
\caption{An example of how recharge modes work in a piecewise function. From the figure we see how the absolute point to which we charge $x_2$ is used together with the previous amount $x_1$ in order to calculate the relative time. By subtracting the previous amount from the newly charged amount, we account for the difference in modes such that the time we pay for the recharge is accurate.}
\label{fig:piecewiseexample}
\includegraphics[width=10cm]{rechargemodesexamplebig.pdf}
\end{figure}
Next, in order to relate our new flexible way of recharging to time it is necessary to know which recharge mode we are in, in order to determine the rate at which we recharge over time. A mode represents a line piece in a piecewise function (See Fig. \ref{fig:piecewiseexample}). A line piece consists of two thresholds, a start point and an end point. These describe the thresholds via their x-values, representing the amount of recharge. Determining which mode we are in is therefore the case of determining which x-intervals we are between with the given battery level. Once the mode is determined, then we know which line piece our battery level is on. From this we can use the slope and y-intercept as the function of the line and determine the y-value, or time spent in order to reach that level of recharge. The next couple of constraints deal with setting the recharge mode of a given amount of battery level using the recharge mode variables.
\begin{align}
\sigma_{\omega}z_{i j \omega}^m \geq o_{ij} \qquad  &\forall j \in N, \ \forall \omega \in \Omega, \forall i \in M
\label{eq:mip22-varfull}\\
\sigma_{\omega-1}z_{i j \omega}^m \leq o_{ij} \qquad  &\forall j \in N, \ \forall \omega \in \Omega, \forall i \in M 
\label{eq:mip23-varfull}\\
\sum_{\omega \in \Omega}z_{i j \omega}^m = 1 \qquad &\forall j \in N, \forall i \in M
\label{eq:mip24-varfull}
\end{align}
Constraints \eqref{eq:mip22-varfull} to \eqref{eq:mip24-varfull} set the mode for a given battery charge level after performing task $j$. Constraints \eqref{eq:mip22-varfull} and \eqref{eq:mip23-varfull} set the mode variable based on the given thresholds for the mode in the piecewise linear function. Constraints \eqref{eq:mip24-varfull} ensures only one mode can be assigned to at a time.
Now that we know the mode for the previous battery level we can compute the time that would be spent on charging from zero capacity to that specific point. We need to know this duration in order to compute the relative time spent on recharging. This is done by using the absolute time for the recharge, $f^t$ and subtract the time spent on the previous level from that.
\begin{align}
c_{ik}^t \geq f^t - H_{\omega}o_{ij} +\eta_\omega- P(1- z_{i j \omega}^m) \qquad \begin{split}&\forall i \in M, \ \forall j \in N, \\ &\forall k \in N, \ \forall \omega \in \Omega\end{split} \label{eq:mip25-varfull}
\end{align}
In Constraints \eqref{eq:mip25-varfull} we set the time variable $c_{ik}^t$ by taking the the absolute time to which we recharge to $f^t$ and the time already accounted for from the old battery level before the recharge and subtract the latter from the first.
Now that we have the time spent recharging, all that is left is to add the variable to the completion time relation involving recharges described in \eqref{eq:mip27}.
\begin{align} C_k - C_{j} + P(1 - x_{ijk}) + P(1 - z_{ijk})\geq p_k + s_{j0} + s_{0k} + c_{ik}^t \qquad \begin{split}&\forall i \in M ,\ \forall j \in N_0,\\ &\forall k_1 \in N,\ j \neq k \end{split} \label{eq:mip27-varfull}
\end{align}
Finally, we describe the changed domains.
\begin{align}
c_{ik}^t \geq 0 \qquad &\forall i \in M, \ \forall k \in N\\
z_{i j \omega}^m \in \{0,1\} \qquad \begin{split}&\forall i \in M, \ \forall j \in N, \\ &\forall \omega \in \Omega\end{split} 
\end{align}
The full model can be seen in Fig. \ref{fig:mipfull}.
\begin{figure}
\caption{Basic model with flexible full recharging added.}
\label{fig:mipfull}
\begin{equation*}\text{minimize } C_{\max} 
\end{equation*}
\begin{align*}  
\sum_{\substack{j \in N_{0} 
\\ j \neq k}} \sum_{i \in M} x_{ijk} = 1 \qquad &\forall k \in N \\
\sum_{\substack{j \in N_{1} 
\\ j \neq k}} \sum_{i \in M} x_{ikj} = 1 \qquad & \forall k \in N \\
\sum_{k\in N_{1}} x_{i0k} = 1\qquad & \forall i \in M \\
\sum_{j\in N_0} x_{ijn+1} = 1\qquad & \forall i \in M \\
\sum_{\substack{k \in N_{1} 
\\ j \neq k}}  x_{ijk}  - \sum_{\substack{h \in N_0 \\ j \neq h}}  x_{ihj} = 0\qquad & \forall j \in N,\ \forall i \in M\\
x_{ijk}\geq z_{ijk}\qquad &  \forall j \in N_0, \ \forall k \in\ N_{1},\ \forall i \in M \\
o_{ik}  \leq f- c_i^- + P(1 - x_{ijk})+P(1-z_{ijk}) \qquad \begin{split}&\forall i \in M, \ \forall j \in N_0,\\
&\forall k \in N_1, j \neq k\end{split}\\
o_{ik}  \leq o_{ij} - c_i^- + P(1 - x_{ijk}) +P(z_{ijk})\qquad \begin{split}&\forall i \in M, \ \forall j \in N_0,\\
&\forall k \in N_1, j \neq k\end{split}\\
o_{ik} \leq f \qquad &\forall i \in M, \ \forall k \in N  \\
o_{ik} \geq q  \qquad &\forall i \in M, \ \forall k \in N \\
o_{i0}  = l_i \qquad &\forall i \in M \\
\sigma_{\omega}z_{i j \omega}^m \geq o_{ij} \qquad  &\forall j \in N, \ \forall \omega \in \Omega, \forall i \in M\\
\sigma_{\omega-1}z_{i j \omega}^m \leq o_{ij} \qquad  &\forall j \in N, \ \forall \omega \in \Omega, \forall i \in M \\
\sum_{\omega \in \Omega}z_{i j \omega}^m = 1 \qquad &\forall j \in N, \forall i \in M\\
c_{ik}^t \geq f^t - H_{\omega}o_{ij} +\eta_\omega- P(1- z_{i j \omega}^m) \qquad &\begin{aligned}\forall i \in M, \ \forall j \in N, \\ \forall k \in N, \ \forall \omega \in \Omega\end{aligned}\\
\begin{aligned}C_k - C_{j} + P(1 - x_{ijk}) + P(1 - z_{ijk}) \\ \geq p_k + s_{j0} + s_{0k} + c_{ik}^t \end{aligned}\qquad &\begin{aligned} \forall i \in M ,\ \forall j \in N_0,\\ \forall k_1 \in N,\ j \neq k \end{aligned}\\
C_k - C_{j} + P(1 - x_{ijk}) \geq p_k + s_{jk}\qquad \begin{split}&\forall i \in M ,\ \forall j \in N_0,\\ &\forall k \in N_{1},\ j \neq k\end{split}\\
C_j \leq d_j \qquad &\forall j \in N \\
C_j - p_j \geq g_j \qquad &\forall j \in N\\
C_j \leq C_{\max} \qquad &\forall j \in N\\
o_{ij} \geq 0 \qquad &\forall i \in M, \ \forall j\ \in N_s \\
z_{ijk} \in \{0,1\} \qquad &\forall j \in N_0 \ \forall k \in\ N_{1} ,\ \forall i \in M\\
x_{ijk} \in \{0,1\} \qquad &\forall i \in M ,\ \forall j \in N_0,\ \forall k \in N_{1} \ j \neq k \\ 
c_{ik}^t \geq 0 \qquad &\forall i \in M, \ \forall k \in N\\
z_{i j \omega}^m \in \{0,1\} \qquad \begin{split}&\forall i \in M, \ \forall j \in N, \\ &\forall \omega \in \Omega\end{split} 
C_{j} \geq 0 \qquad &\forall j \in N 
\end{align*}

\end{figure}
\subsection{Partial Flexible Recharge}
\label{sub:mippartial}
In this subsection we will further modify the model with flexible recharging to full capacity such that we can simply charge what we need at a given time instead.
First, let us introduce the new parameters. We need a way to be able to keep track of the amount we recharge and how much time is spent recharging. For this we define the linear variable $c_{ij}^+$ to replace the parameter $c^+$ used in the fixed recharge model for the amount we recharge. This variable no longer represents some additional amount which must be added to the given battery charge level at some task $j$. Instead, it will represent the level to which we charge to, and therefore the battery charge level of the vehicle $i$ before performing task $j$. We keep the time variable $c_{ij}^t$ for the time spent on recharging. Finally, we add an index to the binary variable $z_{i j \omega v}^m$. We do this because we now need to determine the mode for $c_{ij}^+$ in addition to $o_{ij}$. The $v$ index is for whether or not this is a recharge level for a vehicle $o_{ij}$ ($v=0$) or a recharge amount given by $c_{ij}^+$ ($v=1$).
\bigskip
\par
Now that we have introduced the new parameters and variables, we will move on to the relations. First, we update Equations \eqref{eq:mip20-varfull} and \eqref{eq:mip21-varfull} by replacing the constant $f$ representing full recharge with the new variable. We keep the split relations, one for recharging and one for no scheduled recharge task.
\begin{align} o_{ik}  \leq c_{ik}^+- c_i^- + P(1 - x_{ijk})+P(1-z_{ijk}) \qquad \begin{split}&\forall i \in M, \ \forall j \in N_0,\\
&\forall k \in N_1, j \neq k\end{split}  \label{eq:mip20-var}\\
o_{ik}  \leq o_{ij} - c_i^- + P(1 - x_{ijk}) +P(z_{ijk})\qquad \begin{split}&\forall i \in M, \ \forall j \in N_0,\\
&\forall k \in N_1, j \neq k\end{split}  \label{eq:mip21-var}
\end{align}
Equation \eqref{eq:mip20-var} is for the case of having a recharge task scheduled. Since $c_{ik}^+$ replaces $f$ as the new point to which we recharge. Equation \eqref{eq:mip21-var} stays the same and deals with just normal tasks being scheduled.
\medskip
\par
Next, we change the constraints dealing with setting recharge modes such that they include the index for what kind of recharge variable we are setting, battery level $o_{ik}$ or recharged  level $c_{ik}^+$. First, the constraints for battery level.
\begin{align}
\sigma_{\omega}z_{i j \omega 0}^m \geq o_{ij} \qquad  &\forall j \in N, \ \forall \omega \in \Omega, \forall i \in M
\label{eq:mip22-var}\\
\sigma_{\omega-1}z_{i j \omega 0}^m \leq o_{ij} \qquad  &\forall j \in N, \ \forall \omega \in \Omega, \forall i \in M 
\label{eq:mip23-var}\\
\sum_{\omega \in \Omega}z_{i j \omega 0}^m = 1 \qquad &\forall j \in N, \forall i \in M
\label{eq:mip24-var}
\end{align}
They stay the same as in \eqref{eq:mip22-varfull} to \eqref{eq:mip24-varfull}, except we add the index for variable type.
In order to do the same for $c_{ij}^+$ we simply replace $o_{ij}$ and increment the index for the mode variable.
\begin{align}
\sigma_{\omega}z_{i j \omega 1}^m \geq c_{ij}^+  \qquad  &\forall j \in N, \ \forall \omega \in \Omega, \forall i \in M
\label{eq:mip22.1-var}\\
\sigma_{\omega-1}z_{i j \omega 1}^m \leq c_{ij}^+  \qquad  &\forall j \in N, \ \forall \omega \in \Omega, \forall i \in M 
\label{eq:mip23.1-var}\\
\sum_{\omega \in \Omega}z_{i j \omega 1}^m = 1 \qquad &\forall j \in N, \forall i \in M
\label{eq:mip24.1-var}
\end{align}
Now that we have the recharge modes for both the old level of charge at task $j$ and for the newly generated level of charge just before we perform task $k$, we can compute the time spent recharging by subtracting the time spent on the two in order to get the relative time.
\begin{align}
c_{ik}^t \geq H_{\omega'}c_{ik}^+ +\eta_\omega' - H_{\omega}o_{ij} +\eta_\omega- P(1- z_{i j \omega  0}^m)- P(1-z_{i k \omega' 1}^m) \qquad \begin{split}&\forall i \in M, \ \forall j \in N, \\ &\forall k \in N, \ \forall \omega,\omega' \in \Omega\end{split} \label{eq:mip25-var}
\end{align}
Constraints \eqref{eq:mip23-var} sets the time spent recharging using the piecewise linear function representing the recharge modes. The relative time is computed by first computing the time that would be spent at the old battery charge level and at the generated and subtracting the former from the latter. 
Finally, we add the domains to the model.
\begin{align}
c_{ik}^+ \geq 0 \qquad &\forall i \in M, \ \forall k \in N\\
c_{ik}^t \geq 0 \qquad &\forall i \in M, \ \forall k \in N\\
z_{i j \omega  v}^m \in \{0,1\} \qquad \begin{split}&\forall i \in M, \ \forall j \in N, \\ &\forall \omega \in \Omega, \ \forall v \in \{0,1\}\end{split} 
\end{align}
The full model for full partial recharge can be seen in Fig. \ref{fig:mippartial}.
\begin{figure}
\small
\caption{Basic model with flexible partial recharging added.}
\label{fig:mippartial}
\begin{equation*}\text{minimize } C_{\max} 
\end{equation*}
\begin{align*}  
\sum_{\substack{j \in N_{0} 
\\ j \neq k}} \sum_{i \in M} x_{ijk} = 1 \qquad &\forall k \in N \\
\sum_{\substack{j \in N_{1} 
\\ j \neq k}} \sum_{i \in M} x_{ikj} = 1 \qquad & \forall k \in N \\
\sum_{k\in N_{1}} x_{i0k} = 1\qquad & \forall i \in M \\
\sum_{j\in N_0} x_{ijn+1} = 1\qquad & \forall i \in M \\
\sum_{\substack{k \in N_{1} 
\\ j \neq k}}  x_{ijk}  - \sum_{\substack{h \in N_0 \\ j \neq h}}  x_{ihj} = 0\qquad & \forall j \in N,\ \forall i \in M\\
x_{ijk}\geq z_{ijk}\qquad &  \forall j \in N_0, \ \forall k \in\ N_{1},\ \forall i \in M \\
o_{ik}  \leq c_{ik}^+- c_i^- + P(1 - x_{ijk})+P(1-z_{ijk}) \qquad &\forall i \in M, \ \forall j \in N_0, \
\forall k \in N_1, j \neq k\\
o_{ik}  \leq o_{ij} - c_i^- + P(1 - x_{ijk}) +P(z_{ijk})\qquad &\forall i \in M, \ \forall j \in N_0,\
\forall k \in N_1, j \neq k\\
o_{ik} \leq f \qquad &\forall i \in M, \ \forall k \in N  \\
o_{ik} \geq q  \qquad &\forall i \in M, \ \forall k \in N \\
o_{i0}  = l_i \qquad &\forall i \in M \\
\sigma_{\omega}z_{i j \omega 0}^m \geq o_{ij} \qquad  &\forall j \in N, \ \forall \omega \in \Omega, \forall i \in M\\
\sigma_{\omega-1}z_{i j \omega 0}^m \leq o_{ij} \qquad  &\forall j \in N, \ \forall \omega \in \Omega, \forall i \in M \\
\sigma_{\omega}z_{i j \omega 1}^m \geq c_{ij}^+ \qquad  &\forall j \in N, \ \forall \omega \in \Omega, \forall i \in M\\
\sigma_{\omega-1}z_{i j \omega 1}^m \leq c_{ij}^+ \qquad  &\forall j \in N, \ \forall \omega \in \Omega, \forall i \in M \\
\sum_{\omega \in \Omega}z_{i j \omega v}^m = 1 \qquad &\forall j \in N, \ \forall i \in M, \ \forall v \in \{0,1\}\\
\begin{aligned}c_{ik}^t \geq H_{\omega'}c_{ik}^+ +\eta_\omega'\\ - H_{\omega}o_{ij} +\eta_\omega- P(1- z_{i j \omega  0}^m)- P(1-z_{i k \omega' 1}^m) \end{aligned} \qquad &\begin{aligned} \forall i \in M, \ \forall j \in N, \\ \forall k \in N, \ \forall \omega , \omega' \in \Omega \end{aligned}\\
\begin{aligned}C_k - C_{j} + P(1 - x_{ijk}) + P(1 - z_{ijk}) \\ \geq p_k + s_{j0} + s_{0k} + c_{ik}^t \end{aligned}\qquad &\begin{aligned} \forall i \in M ,\ \forall j \in N_0,\\ \forall k_1 \in N,\ j \neq k \end{aligned}\\
C_k - C_{j} + P(1 - x_{ijk}) \geq p_k + s_{jk}\qquad &\forall i \in M ,\ \forall j \in N_0,\ \forall k \in N_{1},\ j \neq k\\
C_j \leq d_j \qquad &\forall j \in N \\
C_j - p_j \geq g_j \qquad &\forall j \in N\\
C_j \leq C_{\max} \qquad &\forall j \in N\\
o_{ij} \geq 0 \qquad &\forall i \in M, \ \forall j\ \in N_s \\
z_{ijk} \in \{0,1\} \qquad &\forall j \in N_0 \ \forall k \in\ N_{1} ,\ \forall i \in M\\
x_{ijk} \in \{0,1\} \qquad &\forall i \in M ,\ \forall j \in N_0,\ \forall k \in N_{1} \ j \neq k \\ 
c_{ik}^+ \geq 0 \qquad &\forall i \in M, \ \forall k \in N\\
c_{ik}^t \geq 0 \qquad &\forall i \in M, \ \forall k \in N\\
z_{i j \omega  v}^m \in \{0,1\} \qquad \begin{aligned}\forall i \in M, \ \forall j \in N, \\ \forall \omega \in \Omega, \ \forall v \in \{0,1\} \end{aligned}\\
C_{j} \geq 0 \qquad &\forall j \in N
\end{align*}

\end{figure}
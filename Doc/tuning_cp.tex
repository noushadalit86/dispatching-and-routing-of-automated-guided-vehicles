\chapter{Model Tuning}
In this chapter we run initial tests on the CP model and insertion heuristic, from there we try to improve the running time of these models with different techniques. 
In with the CP Model we first explore some of the ways in which we can improve the running time of solving the CP model, this includes different branching strategies as well as symmetry breaking. 
For the insertion heuristic we test combinations of parameters and criteria using a small subset of the test instances.

\section{Constraint Programming}
\label{cp_tuning}
After a problem has been represented as a constraint optimization problem (or constraint satisfaction problem), a constraint programming system implements variables and constraints and provides a solution procedure for the COP, thus looks for an assignment to the variables which satisfies all of the constraints.
Gecode does this with a propagation-based constraint solver, based on exhaustive search.
Propagators realize the constraints of a COP (or CSP) by pruning the variable domains.
By propagation of the constraints, local consistency is restored on each constraint, such that infeasible choices of values from the domains of variables are removed.
Even though some CSPs can be solved using constraint propagation alone (e.g. with Sudokus), constraint propagation alone is incomplete, thus constraint propagation is combined with a search, that splits the problem into smaller problems, that are then solved recursively. This recursion is implemented as a backtracking search.\\
\newline
We here explore different branching strategies, in order to define the shape of the search tree and then specify some symmetries to the model. For the evaluation of the branching strategies as well as the symmetry breaking, we ran the initial tests on a small instance with 6 tasks and 3 vehicles, using flexible recharging, a timelimit of 10 seconds and one available recharge station. All tests are made using the branch and bound search engine.
\newline
Gecode offers different predefined variable-value branching strategies.
A common scheme for branching, could be the following:
\begin{equation}
\mathtt{branch(x, variable \text{ } branching, value \text{ } branching)}
\end{equation}
Here x defines the variable that needs to be branched on. And so the $\mathtt{branch()}$ function, defines the branching strategy (brancher) to be used on x.\\
Here the variable branching strategy, defines which variable is selected for branching, whereas value branching defines which values are selected for branching.\\
We can post multiple branchers, which are then executed in order of creation.\\
In order to find a feasible solution, we specify branching strategies for the following decision variables:
\begin{align*}
Vehicles &\qquad V\\
Start &\qquad S\\
Process &\qquad P\\
Successor &\qquad X\\
Battery &\qquad B\\
Makespan &\qquad z
\end{align*}
The order with the variables and values has a great impact on the shape of the search tree, and thus we start by selecting the order in which the variables are branching, and later try to improve on the value selection scheme.
For this we used the following branchers and then try different ordering of posting the branchers:
\begin{align*}
&\mathtt{branch(vehicles, INT\_VAR\_NONE(), INT\_VAL\_RND(Rnd(seed)))}\\
&\mathtt{branch(successor, INT\_VAR\_NONE(), INT\_VAL\_MIN())}\\
&\mathtt{branch(start, INT\_VAR\_NONE(), INT\_VAL\_MIN())}\\
&\mathtt{branch(process, INT\_VAR\_NONE(), INT\_VAL\_MIN())}\\
&\mathtt{branch(battery, INT\_VAR\_NONE(), INT\_VAL\_MIN())}\\
&\mathtt{branch(makespan, INT\_VAL\_MIN())}
\end{align*}
where $\mathtt{INT\_VAL\_RND(seed)}$ chooses a value according to a random seed, we use a random seed of $1337$.
The following table shows the results of the test, $time opt. found$ is the time it takes to find the optimal solution, and $time opt. proven$ is the time it takes to explore the whole search tree.
\begin{table}[H]
\centering
\caption{Running branching tests with different branchers being posted first on the instance with 6 tasks and 3 vehicles}
	\begin{tabular}{|l|l|l|l|l|} \hline
	Variable ordering & Nodes & Propagators & Time opt. found & Time opt. proven \\ \hline
	$(X,V,B,S,P,z)$ & 200 	& 18929 		& 0.176 sec 			& 1.480 sec\\\hline
	$(V,X,B,S,P,z)$ & 9134 	& 620683 		& 5.86 sec 				& \\\hline
	$(B,V,X,S,P,z)$ &  		& 	 			& 	 					& \\\hline
	$(S,B,V,X,P,z)$ & 	 	&  				&  						& \\\hline
	$(P,S,B,V,X,z)$ &  		&  				&						& \\\hline
	$(z,P,S,B,V,X)$ &  		&  				&  						& \\\hline
	\end{tabular}
\end{table}
From the table we conclude that first posting a brancher on $successor$, outperforms all other posting of first branchers.
This is likely due to the sparse amount of values in the domain of all variables in $succesor$ and the fact that assignment of values allows for pruning of values in all other sets of decision variables.
Since a successor to a task must be scheduled on the same vehicle, the start time of a successor is bounded by the end time of the predecessor as well as the battery level after execution also depends on the predecessor.\\
We further explore ordering of the branching, using an instance with 7 tasks and 3 vehicles, posing the branching for $succesor$ first:
\begin{table}[H]
\centering
\caption{Running branching tests with different ordering of branchers on an instance with 7 tasks and 3 vehicles}
	\begin{tabular}{|l|l|l|l|l|}\hline
Variable ordering & Nodes & Propagators & Time - Opt. sol. found & Time - Opt sol. proven \\\hline
$(X,V,S,P,B,z)$ & 5108 	& 553645 	& 1.1667 sec & 30.4375 sec\\\hline
$(X,P,V,S,B,z)$ & 5108 	& 553645 	& 1.1714 sec & 30.5821 sec\\\hline
$(X,B,P,V,S,z)$ & 5105 	& 573244 	& 1.8026 sec & 30.6537 sec\\\hline
$(X,S,B,P,V,z)$ & 7226 	& 696530 	& 1.1967 sec & 30.4375 sec\\\hline
$(X,z,S,B,P,V)$ & 6687 & 748915 	& 1.2031 sec & 32.5514 sec\\\hline
	\end{tabular}
\end{table}
From this, we see an equal performance of using $vehicles$, $process$ and $start$ as the second decision variable set to be branched on,  but as the number of tasks in an instance, we assume that branching on vehicles first will perform best, and such this is selected.
Thus the branchers are posted in the following order:
\begin{equation}
(X,V,P,S,B,z)
\end{equation}
\newpage
Gecode offers many possibilities of variable and value selection schemes when branching (see \cite{GecodeModeling2015} Chapter 8).\\
Due to the nature of the instances, and the the first task in an instance corresponds to the first variable in the model and likewise for vehicles, no assumptions can be made on which we can base a variable selection scheme.\\
Thus the variable selction scheme is to select the first unassigned variable for all the decision sets. This is achieved in Gecode using $\mathtt{INT\_VAR\_NONE()}$.
For the value selection scheme of the decision variables in $vehicles$, we try to evenly distribute the tasks among the vehicles, thus a random value selection scheme is used $\mathtt{INT\_VAL\_RND(Rnd(seed))}$.
For the value selection of $successor$, we try to select the minimum value and thus quickly finding a feasible solution, on which the rest of the search three can be base: $\mathtt{INT\_VAL\_MIN()}$.\\
Thus all branching is done as follows:
\begin{align*}
&\mathtt{branch(successor, INT\_VAR\_NONE(), INT\_VAL\_MIN())}\\
&\mathtt{branch(vehicles, INT\_VAR\_NONE(), INT\_VAL\_RND(Rnd(seed)))}\\
&\mathtt{branch(start, INT\_VAR\_NONE(), INT\_VAL\_MIN())}\\
&\mathtt{branch(process, INT\_VAR\_NONE(), INT\_VAL\_MIN())}\\
&\mathtt{branch(battery, INT\_VAR\_NONE(), INT\_VAL\_MIN())}\\
&\mathtt{branch(makespan, INT\_VAL\_MIN())}
\end{align*}
We also tried to impose a brancher for minimizing the setup times between tasks across all vehicles. Recall that the following constraint is imposed:
\begin{equation}
\label{cp:ex:10}\mathtt{circuit(ST,X,T,CC)}
\end{equation}
Where $ST$ is a matrix consisting of all setup times between tasks,
$T$ define the actual assigned setup time of a task to its successor. Such that $t_j = 
st_{jk}$ if task $k$ is the successor to task $j$. $CC$ is the total circuit cost of the edges in the circuit.
We then branch on $CC$, selecting the variable with max-regret and values not greater than mean of smallest and largest value:
\begin{align*}
\mathtt{branch(*this, costs, INT\_VAR\_REGRET\_MAX\_MAX(), INT\_VAL\_MIN())}
\end{align*}
This did however not improve the performance.\\
\newline

Another way to limit the search tree, is to look at symmetries in solutions. 
Since the battery recharges are identical in their function, any permutation of the battery recharge variables are symmetric in that they are valid solutions and give the same value with regards to the objective function.
Gecode offers automatic symmetry breaking with Lightweight Dynamic Symmetry Breaking (LDSB), where the symmetries are specified as part of the branch function.
The symmetries are parsed to the brancher in an object of type $Symmetries$:
\lstset{
backgroundcolor=\color{lbcolor},
    tabsize=4,    
%   rulecolor=,
    language=[GNU]C++,
        basicstyle=\scriptsize,
        upquote=true,
        aboveskip={1.5\baselineskip},
        columns=fixed,
        showstringspaces=false,
        extendedchars=false,
        breaklines=true,
        prebreak = \raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}},
        frame=single,
        numbers=left,
        showtabs=false,
        showspaces=false,
        showstringspaces=false,
        identifierstyle=\ttfamily,
        keywordstyle=\color[rgb]{0,0,1},
        commentstyle=\color[rgb]{0.026,0.112,0.095},
        stringstyle=\color[rgb]{0.627,0.126,0.941},
        numberstyle=\color[rgb]{0.205, 0.142, 0.73},
%        \lstdefinestyle{C++}{language=C++,style=numbers}’.
}
\begin{lstlisting}
Symmestries syms;
syms << VariableSymmetry(succesor.slice(n,1,r));
branch(successor, INT_VAR_NONE(), INT_VAL_MIN(), syms)};
\end{lstlisting}
The slice selects only the battery variables and gives them to the $Symmetries$ object, which is then posted as part of the brancher.\\

There also exist symmetries in the selection of tasks to vehicles, given that the vehicles have the same initial parameter, permutations of the scheduling of these tasks results in the same value of the solution. This can be resolved by fixing the successor of the start task for all vehicles with the same initial parameters, as shown by the following code:
\begin{lstlisting}
for (int i = 0; i < m; i++) {
        for (int j = i; j < m; j++) {
            if (i != j 
            	&& ins.getVehicleStartTimes()[i] 
            	== ins.getVehicleStartTimes()[j]
            	&& ins.getInitialBattery()[i] 
            	== ins.getInitialBattery()[j]) {
                rel(*this, succ[i+r+n] <= succ[j+r+n]);
            }
        }
    }
\end{lstlisting}
The results from using these symmetries can be seen in the offline test of the CP (\autoref{cp_offline}), running without and with symmetries on the selected best branching strategy.\\
\newline
Another way to improve the search by having several threads exploring different parts of the search tree in parallel, and thus distributing the computation among several workers. This could improve the performance significantly, however it is not a guarantee, since idle threads might slow down busy threads by the overhead of trying to gain work.
Gecode only does computation on a single processor, thus computation can only be distributed among workers on the same processor. The machine on which the test were made has two threads available per CPU, and thus we use two workers, which showed a spike in performance. And this is used in all further tests of the CP model.\\
\newline
Even though we explored some of the options offered to improve the search and propagation of the model, there are still several ways in which the propagation and search of the model can be improved.
An example could be by ordering the instance variables for tasks and vehicles based on different criteria, e.g. task deadlines, battery consumption or processing times,
And thereby branching on tasks with a high or low value of these criteria.
Another option is to implement custom propagators, which could improve the pruning of variable domains.
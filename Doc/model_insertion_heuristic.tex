\chapter{Insertion Heuristic}
Another approach to solving the VCPSP is to use heuristics. This is a practical method which does not guarantee the solution to be optimal.
Generally heuristic methods use an educated guess to produce the solution.
\newline
We introduce a heuristic algorithm based on a sequential insertion heuristic presented by M. Solomon \cite{Solomon87algorithmsfor} (1987), which solves the vehicle routing and scheduling problem with time window constraints (VRSPTW).\\
\newline
Define the parameters as stated for the CP model (Chapter \ref{cp}).
Let a route of a vehicle be the order and time in which the different tasks are scheduled on a vehicle.
The algorithm first initializes every route using the one of initialization criteria $ci$ described in the section (\ref{heu:init}), and then continues to inserts a new task into a partial route, between two adjacent tasks $j$ and $k$, on a selected route $i$, until all tasks have been scheduled.
The heuristic chooses a single criteria for each of the insertion steps, and keeps using the same criteria until all unrouted tasks are inserted, or no feasible insertions can be found.
The criteria used to decide on which route and where to insert the tasks, is described in section \ref{heu:ins}.\\
\newline
Let $cr(i)$, $cp(i,u,j)$ and $ct(i,u,j)$ be the three criteria used at every iteration, to evaluate where to insert a task $u$ into route $i$ after task $j$.
let $(u_1,\ldots,u_p)$ be the unrouted tasks still to be inserted into the schedule.
First we use the criteria $cr(i)$ to compute the best route $i^*$ to insert into. Next for each unrouted task $u$, we compute the best feasible insertion spots $(j_{u_1},\ldots,j_{u_p})$ in the route with $cp(i^*,j,u)$.
Lastly we use $ct(i^*,u,j_u)$ to calculate the best feasible task $u^*$, which to insert after task $j_{u^*}$ into route $i^*$.\\
\newline
The insertion scheme can thus be split into the following three steps:\\
\begin{algorithm}[H]
	\KwData{Set of tasks and routes, selection criteria $cr(i)$, $cp(i,u,j)$ and $ct(i,u,j)$}
	\KwResult{The route $i^*$ and task $j_{u^*}$ after which to insert task $u^*$}
		\nl$\label{heu:ins:1}i^* \leftarrow$ Best route $i^*$ to insert a task into, selected based on criteria $cr(i)$\\
		\nl$\label{heu:ins:2}j_{u_1},\ldots,j_{u_p} \leftarrow$ Best feasible insertion place in route $i^*$ for all unrouted tasks according to criteria $cp(i^*,u,j)$\\
		\nl$\label{heu:ins:3}u^*,j_{u^*} \leftarrow$ task $u^*$ to insert after task $j_{u^*}$ on route $i^*$ based on the criteria $ct(i^*,u,j_u)$
\caption{Insertion Scheme}
\label{heu:ins:alg}
\end{algorithm}
First step \eqref{heu:ins:1} selects a route $i^*$ based on the criteria $cr(i)$, 
\eqref{heu:ins:2} computes the best feasible insertion place for all unrouted tasks, according to the selection criteria $cp(i^*,u,j)$
and \eqref{heu:ins:3} selects the best task $u^*$ and its insertion place $j_{u^*}$, of all previously computed tasks, to be inserted based on criteria $ct(i^*,u,j_{u})$.\\
Note that in this model, we ignore the constraints of battery capacity when inserting tasks into the schedule, but instead addresses the battery capacity constraints afterwards, by inserting battery recharges and thereby trying to create feasible solutions. This might not always be possible, since an insertion could alter the end time of successive tasks, and ultimately push an end time beyond the tasks deadline. In that case no feasible solutions can be made with that particular combination of criteria.\\
\newline
Let $(s, 1, 2, \ldots, \ell)$ be the chosen partial route to insert the next task into, with $s$ and $\ell$ being start and end (last) tasks. The start task $s$ has the initial battery capacity $l_i$ given that vehicle $i$ is the one servicing the route. Both start and end have zero processing time and battery consumption. The deadline $d_s$ and $d-_\ell$ is given by the upper bound of the solution $U$ and the arrival time of start $at_s$, is given by the vehicle busy time $bs_i$, thus $at_s = bs_i$. Let $s$ be located at the depot, and thus let the setup time from $s$ to the successor task $x_s$ be as from the depot to $x_s$.
Depending on the input, each vehicle go to the depot at the end of its route, thus the setup time to $\ell$ from its predecessor task $y_\ell$ will be as from it to the depot.\\
\newline
Let $j$ and $k$ be two tasks on the schedule, where task $k$ is the successor to task $j$ in the schedule.
Given the insertion of a task $u^*$ between $j$ and $k$,
the successor to task $j$ is now $u^*$ and the successor of $u^*$ is then $k$. Thus the schedule now has a setup time between task $j$ and $u^*$ and also between task $u^*$ to $k$.
Task $k$ could potentially have a different starting and end time as well as battery capacity, compared to task $k$. All these changes depends on the attributes of task $u^*$.\\
\newline
Note that by inserting a task $u^*$ between two tasks $j$ and $k$, we could also potentially alter the times at which all successive tasks in the schedule $(k, \ldots, \ell)$ starts executing, their battery capacity as well as their end time.\\
\newline
Let $\tau_k$ be the idle time of task $k$, this is defined as the time after the setup time of the to task $k$ is completed and task $k$ starts executing.
This can occur if task $k$ has an arrival $at_k$ time higher than the deadlines of all other tasks, and there after setup time to task $k$ is time, where the vehicle is idle, waiting for task $k$ to be available for execution.\\
Recall from the CP model, that $y_k$ defines the predecessor to task $k$, $e_k$ is the end time of task $k$, $s_k$ defines the start time of task $k$ and $s_{y_kk}$ is the setup time between the predecessor $y_k$ and task $k$. We can then define $\tau_k$ as:
\begin{equation}
\tau_k = s_k - (e_{y_k} + s_{y_kk}) \qquad \forall k = 1,\ldots,\ell
\end{equation}
Let a $push$ $forward$ ($PF$) in the schedule be defined as an insertion of $u^*$, let the new starting time of task $k$ be defined by $s_{k^*}$. A $push$ $forward$ in the schedule at $k$ is thus defined as:
\begin{equation}
PF_{k} = s_{k^*} - s_k \geq 0
\end{equation}
Also,
\begin{equation}
\label{heu:1}PF_{o+1} = \max\{0,PF_o-\tau_{o+1}\}, \qquad k \leq o \leq \ell-1
\end{equation}
That is, the $push$ $forward$ of task $o+1$, $k \leq o \leq \ell-1$, depends on the $push$ $forward$ of the previous task $o$.
Furthermore, we can define a $push$ $backward$ in the schedule at $k$. However such a $push$ $backward$ is often not relevant, since a task can only be pushed back if there exist idle time in the schedule, and as we at initialization of the routes, schedule the first task as soon as possible. Let $bs_i$ denote this earliest possible time that the vehicle on route $i$ can start processing the schedule of the route.\\
\newline
In the case that $PF_o > 0$, some of the tasks $o$, $k \leq o \leq \ell-1$ could become infeasible. Thus in order to ensure feasibility of each task $o$, $k \leq o \leq \ell-1$ after a $push$ $forward$, we have to ensure that:
\begin{equation}
\label{heu:2}e_o + PF_{o} \leq d_o, \qquad \forall o = k,\ldots,\ell-1
\end{equation}
Define $b_o$ to be the battery capacity of the vehicle $i$ servicing the route that task $o$ is scheduled on, after task $o$ is finished. Also let $bc_k$ be the battery consumption of task $k$.
Since the heuristic ignores battery consumption when inserting tasks, the following is not enforced:
\begin{equation}
\label{heu:3}q \leq b_o-bc_k \leq f, \qquad \forall o = k,\ldots,\ell-1
\end{equation}
Equation \eqref{heu:2} says that the $push$ $forward$ should not push the task execution past the deadline of the task. Equation \eqref{heu:3} ensures that the battery capacities after the $push$ $forward$ is within lower and upper bound. However as stated, equation \eqref{heu:3} is not tested in the insertion scheme of the heuristic, but instead inforced after all unrouted tasks have been inserted. This is covered in detail in section \ref{heu:bat}.
\section{Initialization}\label{heu:init}
The routes are initialized by scheduling an initial unrouted task on each of the routes. The order of the initialization is decided by the routing selection criteria used, and by random in case of ties.
As described before, let each route have a start $s$ and end task $\ell$.\\
%\newline
%The start task $s$ has the initial battery capacity %$l_i$ given that vehicle $i$ is the one servicing the %route. Both start and end have zero processing time %and battery consumption. The deadline is given by the %upper bound of the solution $U$ and the arrival time %of $s$, $at_s$, is given by the vehicle busy time %$bs_i$, thus $at_s = bs_i$. Let $s$ be located at the %depot, and thus let the setup time from $s$ to the %successor task $x_s$ be as from the depot to $x_s$.
%The setup time to the end task $\ell$ is zero.\\
\newline
The task $u^*$ is inserted between the start $s$ and end task $\ell$, at the earliest possible time $bs_i$, given by the the route $i$.
This task is chosen with the following criteria:
\begin{enumerate}
\item\label{heu:init:1} The unrouted task with highest setup time to routed tasks.
\item\label{heu:init:2} The unrouted task with earliest deadline.
\item\label{heu:init:3} The unrouted task with highest battery consumption.
\item\label{heu:init:4} The unrouted task with minimum weighted combination of highest setup time to already routed tasks and high battery consumption.
\end{enumerate}
Criteria \eqref{heu:init:1} tries to take into advantage of the fact that tasks comes in geographical clusters, which would be ideal to schedule on the same route, and such each cluster should be processed on one route. Let $p$ be the list of already routed tasks. Then the task $u^*$ is the one for which:
\begin{equation}
\max(\min(s_{up}))
\end{equation}
That is, for all unrouted tasks, we find their lowest setup time to all routed tasks, and thereafter find the maximum of these.
Criteria \eqref{heu:init:2} seeks to schedule tasks which have an early deadline, such that urgent tasks are scheduled first, and the insertion criteria described later is not limited by deadline. Criteria \eqref{heu:init:3} focuses on scheduling tasks with high battery consumption first, and thus the most constrainted tasks are scheduled first. Criteria \eqref{heu:init:4} tries to use a combination of setup time and battery consumption. Let $i\alpha_1 = [0,1]$ and $i\alpha_2 = [0,1]$, then the task to insert is calculated as:
\begin{equation}
i\alpha_1 \max(\min(s_{up})) + i\alpha_2 bc_u, \qquad \qquad i\alpha_1 + i\alpha_2 = 1\\
\end{equation}
The idea behind scheduling the most constrained tasks first, is to limit the insertion criteria as little as possible.
\newline
\section{Insertion}\label{heu:ins}
The insertion scheme is as described by algorithm \ref{heu:ins:alg}.
\subsection{Route selection}
First we find the best route $i^*$ to insert a task into. The best route is then the one for which:
\begin{equation}
i^* = \min(cr(i)), \qquad i = 1,\ldots,m
\end{equation}
Where $cr(i)$ is the result of applying the given criteria to the route $i$.
The heuristic uses one of the following criteria:
\begin{align}
\label{heu:ins:route:1}cr_1(i) &= b_\ell\\
\label{heu:ins:route:2}cr_2(i) &= e_\ell\\
\label{heu:ins:route:3}cr_3(i) &= |(s,\ldots,\ell)|
\end{align}
Criteria \eqref{heu:ins:route:1} tries to minimize the limitations of battery capacity, by selecting the route with most battery capacity at the end task, criteria \eqref{heu:ins:route:2} tries to select the least burdened route with regards to makespan. Criteria \eqref{heu:ins:route:3} selects the route with least number of tasks, thereby seeking to spread out the tasks evenly among the routes.\\
\newline
\subsection{Insertion place selection}
Next we find the best feasible insertion spot $j$ in the chosen route $i^*$ for each unrouted task.
Let $cp(i^*,j,u)$ be the evaluation of inserting task $u$ after task $j$ on the chosen route $i^*$ according to the criteria $cp$.
The best feasible insertion spot $j^*$ for a task $u$ is then:
\begin{equation}
j^* = \mathtt{optimum} (cp(i^*,j,u)), \qquad j = s,1,\ldots,\ell-1
\end{equation}
In order to define the criteria $cp(i^*,j,u)$, we introduce three auxiliary criteria.
Let $\mu$ = $[0,1]$, this is used to define the importance of the previous setup time before the insertion. Let $s_{x_u}$ define the start time of $x_j$ when $u$ is inserted into the route $i$ after task $j$. Start and setup time that does not depend on the task $u$ refers to the start and setup time before the insertion of task $u$ into the route $i$. The auxiliary criteria are then defined as:
\begin{align}
\label{heu:ins:aux:1}ca_{1}(i^*,j,u) &= s_{ju} + s_{uk} - \mu s_{jk} \qquad k = x_j&, 0 \leq \mu \leq 1\\
\label{heu:ins:aux:2}ca_{2}(i^*,j,u) &= s_{x_u}-s_{k} \qquad k = x_j&\\
\label{heu:ins:aux:3}ca_{3}(i^*,j,u) &= d_u-s_u
\end{align}
Criteria \eqref{heu:ins:aux:1} tries to select a task and insertion place with the most benefit in setup time, here $0 \leq \mu \leq 1$, is used to define the weight of the previous setup time before the insertion.
Criteria \eqref{heu:ins:aux:2} seeks to minimize the $push$ $forward$ of task $x_j$ caused by inserting $u$. This will give the same as using criteria \eqref{heu:ins:aux:1} with $\mu = 0$.
Criteria \eqref{heu:ins:aux:3} tries to maximize the span between start time $s_u$ and deadline $d_u$ of task $u$.\\
\newline
The criteria for finding the best feasible insertion place are then defined as:
\begin{align}
\label{heu:ins:place:1}cp_1(i^*,j,u) &= \alpha_1 ca_{1}(i^*,j,u) + \alpha_2 ca_{2}(i^*,j,u), \qquad \alpha_1 + \alpha_2 = 1\\
cp_2(i^*,j,u) &= \alpha_1 ca_{1}(i^*,j,u) + \alpha_2 ca_{2}(i^*,j,u)+ \alpha_3 ca_{3}(i^*,j,u), \notag\\
\label{heu:ins:place:2}&\alpha_1 + \alpha_2 + \alpha_3 = 1, \alpha_1 \geq 0, \alpha_2 \geq 0, \alpha_3 \geq 0
\end{align}
Criteria \eqref{heu:ins:place:1} tries to gain benefit from savings in the setup time from scheduling tasks on the same route, which is the case with $\alpha = \mu = 1$ and $\alpha_2 = 0$.\\
Note that different values of $\mu$ as well as $\alpha_1$ and $\alpha_2$ could lead to a different selected task and spot for insertion.
Criteria \eqref{heu:ins:place:2} tries to also consider deadlines of tasks when inserting.\\
\newline
\subsection{Task selection}
Lastly we choose the task with the best insertion spot. Let $ct(i^*,j_u,u)$ be the evaluation of insertion place of task $u$ after task $j_u$ on route $i^*$, according to criteria $ct$.
The best task to insert, is thus:
\begin{equation}
u^* = \mathtt{optimum} (ct(i^*,j_u,u)), \qquad u = 1,\ldots,p
\end{equation}
Let $R(i^*)_d(u)$ be the total route distance, defined as the sum of all setup times in the route $i^*$ after the insertion of $u$. Define $R(i^*)_t(u)$ to be the total time of the route $i^*$, after the insertion of $u$, which is equal to the end time of the end task: $R(i^*)_t(u) = \ell_e$. Let $\lambda = [0,1]$.
The criteria used to select the best task to insert are as follows:
\begin{align}
\label{heu:ins:task:1}ct_1(i^*,j,u) &= cp_1(i^*,j,u)\\
\label{heu:ins:task:2}ct_2(i^*,j,u) &= \lambda s_{su} - cp_1(i^*,j,u), \qquad \lambda \geq 0\\
\label{heu:ins:task:3}cr_3(i^*,j,u) &= \beta_1 R(i^*)_d(u) + \beta_2 R(i^*)_t(u), \qquad \beta_1 + \beta_2 = 1, \beta_1 \geq 0, \beta_2 > 0
\end{align}
Criteria \eqref{heu:ins:task:1} is defined to be the same as criteria \eqref{heu:ins:place:1}.
Thus like criteria \eqref{heu:ins:place:1}, criteria \eqref{heu:ins:task:2} also tries to find savings in the setup time from scheduling tasks on the same route with $\alpha = \mu = \lambda = 1$ and $\alpha_2 = 0$.\\
The third criteria \eqref{heu:ins:task:3} aims to select tasks where the insertion minimize a combination of route distance and time.\\
\newline
This insertion scheme allows to insert a task into any place in a route, instead of at the end. It considers setup times, which in practice is the distance between the tasks. The scheme also accounts for limitations of the tasks and vehicles executing the routes.

\section{Battery Recharge Insertion}\label{heu:bat}
As stated, the insertion scheme is repeated until there either exist no more unrouted tasks, or no feasible insertions can be found with the remaining tasks.
In the case that no feasible insertions can be found with remaining unrouted tasks, no feasible solution can be construct with the selected combination of criteria and capacity. This can happen, if $e_\ell = U$, that is, the end time of $\ell$ is equal to the upper bound of the solution and the insertion of task $u^*$, into the schedule causes a $push$ $forward$ for $\ell$, such that the new end time is higher than its deadline $U$.\\
This could also be for any other tasks $u*$, where the $push$ $forward$ causes the new end time of the task to be higher than its deadline.\\
\newline
Otherwise we arrive at a point where the only thing that makes the solution infeasible, is the battery capacities of at least one task $b_j$ not being above the lower bound $q$of the battery capacity.\\
\newline
Let $r$ be a battery recharge task that takes the battery capacity at the route to max capacity.
$b_j$ be the battery capacity of the vehicle running the route at which task $j$ is scheduled on, after the execution of task $j$. Again let $x_j$ be the immediate successor task to task $j$ in the route.
We then use the following algorithm, to insert battery recharges, until all routes are feasible.\\
\begin{algorithm}[H]
	\KwData{Set of routes $i$ and a schedule of tasks on the routes creating an infeasible solution}
	\KwResult{A feasible or infeasible solution}
\nl\label{heu:bat:alg:1}	\For{Route $i \leftarrow 1$ \KwTo $m$}{
\nl\label{heu:bat:alg:2}		Task $j \leftarrow$ $s$\\
\nl\label{heu:bat:alg:3}		\While{$i$ infeasible $\wedge$ $j \neq i_e$}{
\nl\label{heu:bat:alg:4}			\If{$b_j \leq q$}{
\nl\label{heu:bat:alg:5}				Insert $r$ after $j$\\
\nl\label{heu:bat:alg:6}				Update start, end time and battery capacities of all successive tasks\\
			}
\nl\label{heu:bat:alg:7}			$j \leftarrow x_j$\\
		}
\nl\label{heu:bat:alg:8}		\If{$i$ infeasible}{
\nl\label{heu:bat:alg:9}Terminate with no feasible solution\\}
\nl\label{heu:bat:alg:10}		\If{$b_e > q$}{
\nl\label{heu:bat:alg:11}			Set the last battery recharge inserted, to recharge $b_e-q$ less, update start, end time and battery capacities of all successive tasks to last recharge $r$\\
		}
	}
\caption{Battery Recharge Insertion}
\label{heu:bat:alg}
\end{algorithm}
The algorithm iterates through all routes \eqref{heu:bat:alg:1} and initializes $j$ \eqref{heu:bat:alg:2}.
Then while the route $i$ is infeasible, and $j$ is not the end task \eqref{heu:bat:alg:3}, if the battery capacity after executing task $j$ is below the lower bound of the battery capacity $q$ \eqref{heu:bat:alg:4}, insert $r$ after task $j$ \eqref{heu:bat:alg:5} at the earliest possible time when a battery recharge station is available and update start, end time and battery capacity of all successive tasks \eqref{heu:bat:alg:6}.
Set $j$ to be the next task in the route \eqref{heu:bat:alg:7}.
If after the while loop, $i$ is still infeasible \eqref{heu:bat:alg:8}, no solution can be constructed and we terminate \eqref{heu:bat:alg:9}.
If $i$ on the other hand is feasible, then if we have excess battery at the end task \eqref{heu:bat:alg:10}, eliminate this by recharging that much less at the last battery recharge, and update start, end time and battery capacities of $r$ and all successive tasks \eqref{heu:bat:alg:11}.\\

A solution can still be infeasible after the insertion of battery tasks, the case in which this happens, is the same for when inserting normal tasks, where the $push$ $forward$ of a successive task in the same route as the battery recharge is inserted, causes one of the tasks $u$ to have a new end time $e_u$ to be higher than its deadline $d_u$.

\section{Running time analysis}
In order to compute an upper bound on the running time of the insertion heuristic, we briefly recap the algorithm:\\
Let $UT$ be the set of unrouted tasks.\\
\begin{algorithm}[H]
	\KwData{Set of routes $M$ and tasks $N$}
	\KwResult{A feasible or infeasible solution}
\nl\label{heu:ins:alg:1}	Initialize routes $(ci)$ \\
\nl\label{heu:ins:alg:2}	\While{$UT \neq \emptyset$}{
\nl\label{heu:ins:alg:3}		Select best route using $(cr)$\\
\nl\label{heu:ins:alg:4}		Calculate best feasible insertion places for each unrouted task using $(cp)$\\
\nl\label{heu:ins:alg:5}		Select best task using $(ct)$\\
\nl\label{heu:ins:alg:6}		Insert selected task, into selected route, at selected insertion place and remove the task from the set of unrouted tasks.\\
				}
\nl\label{heu:ins:alg:7}	Insert battery recharges\\
\caption{Insertion Heuristic Algorithm}
\label{heu:ins:alg}
\end{algorithm}
The initialization of routes \eqref{heu:ins:alg:1} is done once. The order, in which the routes are initialized in, is decided by the route selection criteria $cr(i)$, which is the same as the one used by the insertion scheme in line \eqref{heu:ins:alg:3}.
The cost of evaluating the route according to a route selecting criteria is constant, and since the route selection criteria is applied to each route $m$, thus applying the route selection criteria to all routes costs is $O(m)$.
The resulting values are then sorted, which costs $O(m\log()m)$ time, thus the total time of finding the ordering is:
\begin{equation}
O(m) + (O(m\log{}m) = O(m\log{}m)
\end{equation}
After finding the order of route initialization, we have to initialize each of the $m$ routes.
This is done by seleting the best task according to the insertion criteria $ci$. Here $ci_4$ is the most expensive criteria costing $O(nm)+O(n)+O(n)$. The governing part of $ci_4$ which costs $O(nm)$, is the time it takes to find the lowest setuptime for each unrouted task $n$ to each routed task $m$. It then takes $O(n)$ from finding the maximum value among these and lastly $O(n)$ from getting the battery consumption for each of the unrouted tasks.
This criteria is used on each of the $m$ routes to be initialized. The resulting task is then inserted into the empty route, which takes constant time. Thus total cost of the initialization \eqref{heu:ins:alg:1} is given by:
\begin{equation}
O(m\log{}m) + m(O(nm)+O(n)+O(n)) = O(m\log{m}) + O(nm^2) = O(m^2)
\end{equation}
After the initialization of routes, we have $n-m$ unrouted tasks left to schedule, thus the while loop executes 
$O(n)$ times.\\
As described before, the running time of selecting a route based on the route selection criteria \eqref{heu:ins:alg:3} is $O(m)$.
When calculating the best feasible insertion place for each task \eqref{heu:ins:alg:4},
The evaulation of a single inseriton place could potentially cause a $push$ $forward$ of all successive tasks in the schedule, thus all successive $O(n)$ tasks in the schedule are checked for feasibibility in constant time, regardless of which insertion place criteria $cp$ is used.
All possible insertions places for all unrouted tasks on the selected route are evuated. All scheduled tasks could be placed on this route, thus we have $O(n)$ potential insertion places to evalute for $O(n)$ unrouted tasks.
Thus the total time of \eqref{heu:ins:alg:4} is:
\begin{equation}
O(n) \cdot O(n) \cdot O(n) = O(n^3)
\end{equation}
When evaluating criteria $cp$ in \eqref{heu:ins:alg:4}, an evaluation of criteria $ct$ used in \eqref{heu:ins:alg:5} can also be made. Since the $push$ $forward$ is already considred when evaluating $cp$, the task selection criteria $ct$ can be evaluated in constant time. Also when the feasiblity check of all tasks influenced by the $push$ $forward$ occurs, the potential new values for these tasks is saved, which makes the insertion of this task constant.
It also takes constant time to remove the inserted task from the list of unrouted tasks.
\begin{equation}
O(n) \cdot (O(m) + O(n^3) = O(n^4)
\end{equation}
Lastly battery recharges are inserted \eqref{heu:ins:alg:7} to create a feasible solution with respect to the limits of the battery capacity. A route could worst case have been scheduled all tasks $n$, and the battery consumption of the tasks require a battery recharge to be scheduled after the execution of each task, thus $n$ battery recharges have to inserted.
Each insertion could potentially cause a $push$ $forward$ of all successive $O(n)$ tasks.
And battery recharges are inserted into every route $m$ untill they are feasible with respect to the battery limits, the total running time is:
\begin{equation}
m(O(n) \cdot O(n) \cdot O(n)) = O(n^3)
\end{equation}
The total running time $T(n)$ of the insertion heuristic is then given by:
\begin{equation}
T(n) = O(m^2) + O(n^4) + O(n^3) = O(n^4) 
\end{equation}
\section{Recharge feasibility check}
Another approach could be to account for the battery capacity as the routes are constructed.
First calculate the number of needed battery recharges to construct a feasible soltion, this can be done according to the description in \autoref{battery}. And then add these battery recharge tasks to the list of unrouted tasks that needs to be scheduled.\\
Thus at every insertion, the feasiblity of battery capacity has to tested, as stated by equation \eqref{heu:3}.\\
Since battery recharges are inserted when the route is construct, setup times from and to the depot would also be considered as the route is constructed.\\
A battery recharge would then be inserted into the route, when no other feasible tasks are eligible for insertion on the given route. This could either be done after the route is selected based on the route criteria $cr$, or at the start of the iteration of the insertion scheme. The latter would ensure that all routes have enough battery capacity to schedule the routes.
If no feasible solution can be constructed with the given amount battery recharge tasks, and no insertions can be made without violating the lower bound of battery capacity. A battery recharge task could be added to the list of unrouted tasks, or we could terminate and redeem the solution infeasible.